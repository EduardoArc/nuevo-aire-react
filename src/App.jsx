import React from "react";
import Approuter from "./routers/AppRouter";

import Authprovider from "./routers/AuthProvider";
import { SnackbarProvider } from "notistack";

function App() {
  return (
    //Aquí va también el guard para verificar sesión
    <SnackbarProvider maxSnack={4}>
      <Authprovider>
        <Approuter />
      </Authprovider>
    </SnackbarProvider>
  );
}

export default App;
