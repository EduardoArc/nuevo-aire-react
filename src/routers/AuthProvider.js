import {createContext, useState, useEffect} from 'react';



export const AuthContext = createContext();


const Authprovider = ({children}) => {
    
    const [token, setToken] = useState(
        localStorage.getItem("token")
    );

    const [roles, setRoles] = useState(
         JSON.parse(localStorage.getItem("roles"))
    );

    const [user, setUser] = useState(
       localStorage.getItem("user")
   );
    

    useEffect( ()=>{
        //busca al usuario

        //guarda al token y a usuario en local storage
       // localStorage.removeItem("token")
        localStorage.setItem("token",token);
        localStorage.setItem("roles", JSON.stringify( roles));
        localStorage.setItem("user",  user);
    },[token, roles, user] )

  

    const contextValue = {
        token,
        roles,
        user,
        login(tok, roles, user){
            setToken(tok);
            setRoles(roles);
            setUser(user);
            //obtiene usuario

        },
        logout(){
            localStorage.removeItem("token")
            setToken(null)
            setRoles(null)
            setUser(null)
            
        },
        isLogged(){
            //return !!token;

            if(token === null || token === "null"){
                return false;
            }
            return true
           
        },
        isAdmin() {
          //  const roles = localStorage.setItem("roles", roles);
            //TODO: recorre roles del usuario hasta encontrar admin
            //getUsuario()

            
            if(roles){
                for(let i = 0; i < roles.length; i++){
                    if(roles[i].name === "admin"){
                        return true
                    }
                    
                }
            }
           
            return false;
        },
        isModerador() {
            //  const roles = localStorage.setItem("roles", roles);
              //TODO: recorre roles del usuario hasta encontrar admin
              //getUsuario()
  
              
              if(roles){
                  for(let i = 0; i < roles.length; i++){
                      if(roles[i].name === "moderador"){
                          return true
                      }
                      
                  }
              }
             
              return false;
          },
          isModeradorOrAdmin() {
            //  const roles = localStorage.setItem("roles", roles);
              //TODO: recorre roles del usuario hasta encontrar admin
              //getUsuario()
  
              if(roles){
                  for(let i = 0; i < roles.length; i++){
                      if(roles[i].name === "moderador" || roles[i].name === "admin"){
                          return true
                      }
                      
                  }
              }
             
              return false;
          },
          isUser() {
            //  const roles = localStorage.setItem("roles", roles);
              //TODO: recorre roles del usuario hasta encontrar admin
              //getUsuario()
  
              
              if(roles){
                  for(let i = 0; i < roles.length; i++){
                      if(roles[i].name === "user"){
                          return true
                      }
                      
                  }
              }
             
              return false;
          }

    }

    return <AuthContext.Provider value={contextValue}>
        {children}
    </AuthContext.Provider>
}

export default Authprovider;
