import React, { useEffect } from "react";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import axios from "axios";

import Login from "../components/Login";
import Register from "../components/Register";

import BaseNav from "../components/BaseNav.jsx";

import Publicroute from "./PublicRoute";
import Privateroute from "./PrivateRoute";
import useAuth from "./useAuth";
import { useHistory } from "react-router-dom";

import { useSnackbar } from "notistack";

const Approuter = () => {
  const auth = useAuth();
  const history = useHistory();

  const { enqueueSnackbar } = useSnackbar();

  const handleAlert = (variant, message) => {
    enqueueSnackbar(message, { variant });
  };

  const verifica = async () => {
    await axios
      .get("http://new-air-api-dev.us-west-2.elasticbeanstalk.com/verify", {
        headers: { "x-access-token": auth.token },
      })
      .then((response) => {
        console.log("es el token correcto", response);
      })
      .catch((error) => {
        if (error.response.status == 401) {
          // history.push("/login");
          handleAlert("error", "La sesión expiró");
          auth.logout();
        }
      });
  };

  useEffect(async () => {
    /*
    const interval = setInterval(async () => {
      await verifica();
    }, 9000);
    return () => clearInterval(interval); */
    await verifica();
  }, []);

  return (
    <div>
      <Router>
        <Switch>
          <Publicroute exact path="/register" component={Register} />
          <Publicroute exact path="/login" component={Login} />
          <Privateroute path="/" component={BaseNav} />

          <Route path="*">
            <p>404</p>
          </Route>
        </Switch>
      </Router>
    </div>
  );
};

export default Approuter;
