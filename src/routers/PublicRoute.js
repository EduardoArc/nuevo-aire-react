import { Route, Redirect } from "react-router-dom";
import useAuth from "./useAuth";

const Publicroute = ({ component: Component, ...rest }) => {

  const auth = useAuth();
  return (
    <Route {...rest}>
      {!auth.isLogged() ? <Component /> : <Redirect to="/menu" />}
    </Route>
  );
};

export default Publicroute;
