import { React, useState, useEffect } from "react";

import { useParams } from "react-router-dom";

import axios from "axios";

import {
  Card,
  CardContent,
  Typography,
  Table,
  TableContainer,
  TableHead,
  TableCell,
  TableBody,
  TableRow,
  Paper,
  Toolbar,
} from "@mui/material";

const Organizaciongrupos = () => {
  const [organizacion, setOrganizacion] = useState([]);
  const [gruposExistentes, setGruposExistentes] = useState([]);
  const { id } = useParams();

  const organizacionesBaseUrl = "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/organizaciones/";
  const GruposBaseUrl = "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/usuarios/";

  const getOrganizacion = async () => {
    await axios.get(organizacionesBaseUrl + id).then((response) => {
      setOrganizacion(response.data);

      console.log(response.data);
    });
  };

  useEffect(async () => {
    await getOrganizacion();
  }, []);

  return (
    <div>
      <Card>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            <h2> {organizacion.nombre}</h2>
          </Typography>
          <TableContainer component={Paper}>
            <Toolbar>
              <Typography
                sx={{ flex: "1 1 100%" }}
                variant="h6"
                id="tableTitle"
                component="div"
              >
                Grupos
              </Typography>
            </Toolbar>

            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Nombre</TableCell>

                  <TableCell>Acciones</TableCell>
                </TableRow>
              </TableHead>
              
              <TableBody>
                {organizacion.grupos &&
                  organizacion.grupos.map((element) => (
                    <TableRow key={element._id}>
                      <TableCell>{element.nombre}</TableCell>
                    </TableRow>
                  ))}

               
                  
              </TableBody>
              
            </Table>
          </TableContainer>
        </CardContent>
      </Card>
    </div>
  );
};

export default Organizaciongrupos;
