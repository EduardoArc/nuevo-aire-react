import React, { useEffect, useState } from "react";
import Card from "@mui/material/Card";
import axios from "axios";
import CardContent from "@mui/material/CardContent";

import Typography from "@mui/material/Typography";

import { Toolbar, Tooltip, Button, IconButton, Chip } from "@mui/material/";
import Paper from "@mui/material/Paper";

//import {AddIcon} from '@mui/icons-material/';
import AddIcon from "@mui/icons-material/Add";
import { Edit, Delete, Check } from "@material-ui/icons";
import GroupsIcon from "@mui/icons-material/Groups";

import {
  Table,
  TableContainer,
  TableHead,
  TableCell,
  TableBody,
  TableRow,
  Modal,
  TextField,
  Box,
  FormGroup,
} from "@material-ui/core";

import {
  BrowserRouter as Router,
  Switch,
  Link,
  useHistory,
} from "react-router-dom";

import useAuth from "../../routers/useAuth";
import { useSnackbar } from "notistack";

const cardStyle = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const baseUrl =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/organizaciones/";
const baseUrlUser =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/usuarios/";

export default function Organizacion() {
  const [organizaciones, setOrganizaciones] = useState([]);
  const [modalInsertar, setModalInsertar] = useState(false);
  const [modalEditar, setModalEditar] = useState(false);
  const [modalEliminar, setModalEliminar] = useState(false);
  const [modalActivar, setModalActivar] = useState(false);

  const [organizacionSeleccionada, setOrganizacionSeleccionada] = useState({
    nombre: "",
    descripcion: "",
  });

  const [leyenda, setLeyenda] = useState({
    nombre: "",
    descripcion: "",
  });
  const [errorTitulo, setErrorTitulo] = useState({
    nombre: false,
    descripcion: false,
  });

  const handleChange = (e) => {
    const { name, value } = e.target;

    setOrganizacionSeleccionada((prevState) => ({
      ...prevState,
      [name]: value,
    }));

    //quita errores de validación
    setLeyenda((prevState) => ({
      ...prevState,
      [name]: "",
    }));

    setErrorTitulo((prevState) => ({
      ...prevState,
      [name]: false,
    }));

    //console.log(organizacionSeleccionada)
  };
  const auth = useAuth();

  const getOrganizaciones = async () => {
    if (auth.isAdmin()) {
      await axios.get(baseUrl).then((response) => {
        setOrganizaciones(response.data);
      });
    } else if (auth.isModerador()) {
      await axios
        .get(baseUrlUser + auth.user, {
          headers: { "x-access-token": auth.token },
        })
        .then((response) => {
          setOrganizaciones(response.data.organizaciones);
        });
    }
  };

  const seleccionarOrganizacion = (organizacion, caso) => {
    setOrganizacionSeleccionada(organizacion);

    if (caso === "editar") {
      setModalEditar(true);
    } else {
      setModalEliminar(true);
    }
  };

  const seleccionarOrganizacionActivar = (organizacion) => {
    setOrganizacionSeleccionada(organizacion);
    setModalActivar(true);
  };

  const { enqueueSnackbar } = useSnackbar();

  const handleAlert = (variant, message) => {
    enqueueSnackbar(message, { variant });
  };

  const addOrganizacion = async () => {
    if (organizacionSeleccionada.nombre.length === 0) {
      setLeyenda((prevState) => ({
        ...prevState,
        nombre: "El nombre es requerido",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        nombre: true,
      }));
    } else if (organizacionSeleccionada.descripcion.length === 0) {
      setLeyenda((prevState) => ({
        ...prevState,
        descripcion: "Descripcion requerida",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        descripcion: true,
      }));
    } else {
      await axios
        .post(baseUrl, organizacionSeleccionada, {
          headers: { "x-access-token": auth.token },
        })
        .then(
          abrirCerrarAgregar(),

          setOrganizacionSeleccionada({
            nombre: "",
            descripcion: "",
          })
        )
        .then((res) => {
          handleAlert("success", res.data.message);
          getOrganizaciones();
        })
        .catch((res) => {
          handleAlert("error", res.data.message);
        });
    }
  };

  const updateOrganizacion = async () => {
    if (organizacionSeleccionada.nombre.length === 0) {
      setLeyenda((prevState) => ({
        ...prevState,
        nombre: "El nombre es requerido",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        nombre: true,
      }));
    } else if (organizacionSeleccionada.descripcion.length === 0) {
      setLeyenda((prevState) => ({
        ...prevState,
        descripcion: "Descripcion requerida",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        descripcion: true,
      }));
    } else {
      await axios
        .put(baseUrl + organizacionSeleccionada._id, organizacionSeleccionada)
        .then(
          abrirCerrarEditar(),
          setOrganizacionSeleccionada({
            nombre: "",
            descripcion: "",
          })
        )
        .then((res) => {
          handleAlert("success", res.data.message);
          getOrganizaciones();
        })
        .catch((res) => {
          handleAlert("error", res.data.message);
        });
    }
  };

  const activaOrganizacion = async () => {
    await axios
      .post(baseUrl + "activa/" + organizacionSeleccionada._id)
      .then(abrirCerrarActivar())
      .then((res) => {
        getOrganizaciones();
        handleAlert("success", res.data.message);
      })
      .catch((res) => {
        handleAlert("error", res.data.message);
      });
  };

  const deleteOrganizacion = async () => {
    await axios
      .delete(baseUrl + organizacionSeleccionada._id)
      .then(abrirCerrarEliminar())
      .then((res) => {
        handleAlert("success", res.data.message);
        getOrganizaciones();
      })
      .catch((res) => {
        handleAlert("error", res.data.message);
      });
  };

  //Gestión del modal
  const abrirCerrarAgregar = () => {
    setModalInsertar(!modalInsertar);
    limpiaValidacion();
    setOrganizacionSeleccionada({
      nombre: "",
      descripcion: "",
    });
  };

  const abrirCerrarEliminar = () => {
    setModalEliminar(!modalEliminar);
  };

  const abrirCerrarActivar = () => {
    setModalActivar(!modalActivar);
  };

  const limpiaValidacion = () => {
    setLeyenda({
      nombre: "",
      descripcion: "",
    });

    setErrorTitulo({
      nombre: false,
      descripcion: false,
    });
  };

  const abrirCerrarEditar = () => {
    setModalEditar(!modalEditar);
  };

  useEffect(async () => {
    await getOrganizaciones();
  }, []);

  const history = useHistory();

  const bodyEditar = (
    <div>
      <Box sx={cardStyle}>
        <h3>Editar Dispositivo</h3>
        <FormGroup>
          <TextField
            variant="outlined"
            label="Nombre"
            name="nombre"
            onChange={handleChange}
            helperText={leyenda.nombre}
            error={errorTitulo.nombre}
            value={organizacionSeleccionada && organizacionSeleccionada.nombre}
          />
          <br />
          <TextField
            variant="outlined"
            label="Descripción"
            name="descripcion"
            onChange={handleChange}
            helperText={leyenda.descripcion}
            error={errorTitulo.descripcion}
            value={
              organizacionSeleccionada && organizacionSeleccionada.descripcion
            }
          />
          <br />

          <Button onClick={updateOrganizacion} color="success">
            Actualizar
          </Button>
          <Button onClick={abrirCerrarEditar} color="error">
            Cerrar
          </Button>
        </FormGroup>
      </Box>
    </div>
  );

  const bodyInsertar = (
    <div>
      <Box sx={cardStyle}>
        <h3>Agregar Organización</h3>
        <FormGroup>
          <TextField
            variant="outlined"
            label="Nombre"
            name="nombre"
            onChange={handleChange}
            helperText={leyenda.nombre}
            error={errorTitulo.nombre}
          />
          <br />
          <TextField
            variant="outlined"
            label="Descripción"
            name="descripcion"
            onChange={handleChange}
            helperText={leyenda.descripcion}
            error={errorTitulo.descripcion}
          />
          <br />

          <Button color="success" onClick={addOrganizacion}>
            Agregar
          </Button>

          <Button onClick={abrirCerrarAgregar} color="error">
            Cerrar
          </Button>
        </FormGroup>
      </Box>
    </div>
  );

  const bodyEliminar = (
    <div>
      <Box sx={cardStyle}>
        <h3>
          ¿Seguro que desea eliminar{" "}
          {organizacionSeleccionada && organizacionSeleccionada.nombre} ?
        </h3>
        <FormGroup>
          <Button
            color="success"
            onClick={() => {
              deleteOrganizacion();
            }}
          >
            Aceptar
          </Button>
          <Button onClick={abrirCerrarEliminar} color="error">
            Cancelar
          </Button>
        </FormGroup>
      </Box>
    </div>
  );

  const bodyActivar = (
    <div>
      <Box sx={cardStyle}>
        <h3>
          ¿Seguro que desea activar{" "}
          {organizacionSeleccionada && organizacionSeleccionada.nombre} ?
        </h3>
        <FormGroup>
          <Button
            color="success"
            onClick={() => {
              activaOrganizacion();
            }}
          >
            Aceptar
          </Button>
          <Button onClick={abrirCerrarEliminar} color="error">
            Cancelar
          </Button>
        </FormGroup>
      </Box>
    </div>
  );

  return (
    <>
      <Card>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            Organizaciones
          </Typography>

          <TableContainer component={Paper}>
            <Toolbar>
              <Typography
                sx={{ flex: "1 1 100%" }}
                variant="h6"
                id="tableTitle"
                component="div"
              >
                Gestión de organizaciones
              </Typography>
              {auth.isAdmin() && (
                <Tooltip title="Agregar organización">
                  <Button
                    onClick={abrirCerrarAgregar}
                    variant="contained"
                    startIcon={<AddIcon />}
                  >
                    Agregar
                  </Button>
                </Tooltip>
              )}
            </Toolbar>

            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Nombre</TableCell>
                  <TableCell>Descripción</TableCell>
                  {auth.isAdmin() && <TableCell>Estado</TableCell>}
                  <TableCell>Acciones</TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {organizaciones.map((element) => (
                  <TableRow key={element._id}>
                    <TableCell>{element.nombre}</TableCell>
                    <TableCell>{element.descripcion}</TableCell>
                    {auth.isAdmin() && (
                      <TableCell>
                        <Chip
                          label={element.estado == 1 ? "Activo" : "Inactivo"}
                          color={element.estado == 1 ? "success" : "error"}
                        />
                      </TableCell>
                    )}
                    <TableCell>
                      {auth.isAdmin() && (
                        <IconButton
                          onClick={() => {
                            seleccionarOrganizacion(element, "editar");
                          }}
                        >
                          <Edit color="primary" />
                        </IconButton>
                      )}
                      {element.estado == 1 && auth.isAdmin() ? (
                        <IconButton
                          onClick={() => {
                            seleccionarOrganizacion(element, "eliminar");
                          }}
                        >
                          <Delete color="error" />
                        </IconButton>
                      ) : element.estado == 0 && auth.isAdmin() ? (
                        <IconButton
                          onClick={() => {
                            seleccionarOrganizacionActivar(element);
                          }}
                        >
                          <Check color="success" />
                        </IconButton>
                      ) : (
                        " "
                      )}

                      <Switch>
                        <Link to={"/organizacion/grupos/" + element._id}>
                          <IconButton>
                            <GroupsIcon />
                          </IconButton>
                        </Link>
                      </Switch>
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </CardContent>
      </Card>

      <Modal open={modalInsertar} onClose={abrirCerrarAgregar}>
        {bodyInsertar}
      </Modal>

      <Modal open={modalEditar} onClose={abrirCerrarEditar}>
        {bodyEditar}
      </Modal>

      <Modal open={modalEliminar} onClose={abrirCerrarEliminar}>
        {bodyEliminar}
      </Modal>

      <Modal open={modalActivar} onClose={abrirCerrarActivar}>
        {bodyActivar}
      </Modal>
    </>
  );
}
