import React from 'react';
import { Route, BrowserRouter as Router, Switch } from "react-router-dom";
import Organizacion from './Organizacion';
import Organizaciongrupos from './OrganizacionGrupos';


const Baseorganizacion = () => {
    return (
        <Router>
            <Switch>
                
                <Route exact path="/organizacion/" component={Organizacion}/>
                <Route exact  path="/organizacion/grupos/:id" component={Organizaciongrupos}/>

            </Switch>
        </Router>
    );
}

export default Baseorganizacion;
