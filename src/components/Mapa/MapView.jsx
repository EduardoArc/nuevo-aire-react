import React, { useEffect, useState } from "react";
import axios from "axios";
import { Map, Marker } from "pigeon-maps";

const dispUrl = "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/dispositivos/";
const Mapview = (props) => {
  
    useEffect(()=>{
        console.log(props)
    },[])

  //const parametros = [-36.61705369615801, -72.13106744870429];
  const parametros = [ parseFloat(props.latitud), parseFloat(props.longitud)];
  return (
    <div>
     
        <Map
          height={300}
          width={450}
          defaultCenter={parametros}
          defaultZoom={14}
        >
          <Marker width={40} anchor={parametros} />
        </Map>
      
    </div>
  );
};

export default Mapview;
