import React, { useEffect, useState } from "react";
import axios from "axios";
import { Map, Marker } from "pigeon-maps";

const dispUrl = "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/dispositivos/";
const Mapcard = (props) => {
  
    

  const parametros = [props.latitud, props.longitud];
  return (
    <div>
     
        <Map
          height={140}
          
          defaultCenter={parametros}
          defaultZoom={14}
        >
          <Marker width={40} anchor={parametros} />
        </Map>
      
    </div>
  );
};

export default Mapcard;
