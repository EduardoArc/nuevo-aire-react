import React from 'react';
import useAuth from '../routers/useAuth';
const Home = () => {
    const auth =  useAuth();
    return (
        <div>
            <h1>Home page works!! {auth.isAdmin()}</h1>
        </div>
    );
}

export default Home;
