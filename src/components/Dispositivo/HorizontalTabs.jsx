import { React, useEffect, useState } from "react";
import PropTypes from "prop-types";
import Tabs from "@mui/material/Tabs";
import Tab from "@mui/material/Tab";
import Typography from "@mui/material/Typography";
import Box from "@mui/material/Box";
import LinearChart from "../Charts/LinearChart";
import CincoAnterioresChart from "../Charts/CincoAnterioresChart";

function TabPanel(props) {
  const { children, value, index, ...other } = props;

  useEffect(() => {
    console.log(props.cincoAnteriores);
  }, []);

  return (
    <div
      role="tabpanel"
      hidden={value !== index}
      id={`simple-tabpanel-${index}`}
      aria-labelledby={`simple-tab-${index}`}
      {...other}
    >
      {value === index && (
        <Box sx={{ p: 3 }}>
          <Typography>{children}</Typography>
        </Box>
      )}
    </div>
  );
}

TabPanel.propTypes = {
  children: PropTypes.node,
  index: PropTypes.number.isRequired,
  value: PropTypes.number.isRequired,
};

function a11yProps(index) {
  return {
    id: `simple-tab-${index}`,
    "aria-controls": `simple-tabpanel-${index}`,
  };
}

export default function BasicTabs(props) {
  const [value, setValue] = useState(0);

  const handleChange = (event, newValue) => {
    setValue(newValue);
  };

  return (
    <Box sx={{ width: "100%" }}>
      <Box sx={{ borderBottom: 1, borderColor: "divider" }}>
        <Tabs
          value={value}
          onChange={handleChange}
          aria-label="basic tabs example"
        >
          <Tab label="Mediciones recientes" {...a11yProps(0)} />
          <Tab label="Promedio días anteriores" {...a11yProps(1)} />
        </Tabs>
      </Box>
      <TabPanel value={value} index={0}>
        <CincoAnterioresChart cincoAnteriores={props.cincoAnteriores} />
      </TabPanel>
      <TabPanel value={value} index={1}>
        <LinearChart promedios={props.promedios} />
      </TabPanel>

      <TabPanel value={value} index={2}>
        Item Three
      </TabPanel>
    </Box>
  );
}
