import React, { useState, useEffect } from "react";
import { experimentalStyled as styled } from "@mui/material/styles";
import Box from "@mui/material/Box";
import Paper from "@mui/material/Paper";
import Grid from "@mui/material/Grid";
import Card from "@mui/material/Card";
import CardActions from "@mui/material/CardActions";
import CardContent from "@mui/material/CardContent";
import Button from "@mui/material/Button";
import Typography from "@mui/material/Typography";
import Mapcard from "../Mapa/MapCard";
import axios from "axios";
import useAuth from "../../routers/useAuth";
import InfoIcon from "@mui/icons-material/Info";
import Modal from "@mui/material/Modal";

import Table from "@mui/material/Table";
import TableBody from "@mui/material/TableBody";
import TableCell from "@mui/material/TableCell";
import TableContainer from "@mui/material/TableContainer";
import TableHead from "@mui/material/TableHead";
import TableRow from "@mui/material/TableRow";
import LinearProgress from "@mui/material/LinearProgress";

import {
  BrowserRouter as Router,
  Switch,
  Link,
  useHistory,
} from "react-router-dom";

const Item = styled(Paper)(({ theme }) => ({
  ...theme.typography.body2,
  padding: theme.spacing(2),
  textAlign: "center",
  color: theme.palette.text.secondary,
}));

const baseUrl =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/dispositivos/";

const dispUrlUser =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/usuarios/dispositivos/";

const dispMed =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/dispositivos/medicion";

//const dispMed = "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/dispositivos/medicion";

const CardItem = (prop) => {
  useEffect(() => {
    console.log("PROP ", prop);
    getDispositivo();
  }, []);

  const [dispositivo, setDispositivo] = useState("");
  const [latitud, setLatitud] = useState("");
  const [longitud, setLongitud] = useState("");

  const getDispositivo = async () => {
    try {
      const data = await fetch(
        "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/dispositivos/" +
          prop.id
      );
      const disps = await data.json();
      // console.log(disps.dispositivo.latitud)
      setDispositivo(disps);
      setLatitud(parseFloat(disps.dispositivo.latitud));
      setLongitud(parseFloat(disps.dispositivo.longitud));
      //  console.log("DISPOSITIVO: ",dispositivo)
    } catch (error) {
      console.log(error);
    }
  };

  return (
    <Card sx={{ maxWidth: 345 }}>
      {longitud != "" && <Mapcard latitud={latitud} longitud={longitud} />}

      <CardContent>
        <Grid justifyContent="space-between">
          <Pm25 pm25={prop.pm25} />

          <Typography gutterBottom variant="h5" component="div" mt={2}>
            {prop.nombre}
          </Typography>

          {dispositivo && (
            <>
              <Typography gutterBottom variant="h6" component="div">
                {dispositivo.organizacion.nombre}
              </Typography>

              <Typography gutterBottom variant="p" component="div">
                {dispositivo.grupo.nombre}
              </Typography>
            </>
          )}
        </Grid>

        <Mensaje pm25={prop.pm25} />
      </CardContent>
      <CardActions>
        <Switch>
          <Link
            exact
            to={"/dispositivo/detalle/" + prop.id}
            style={{ textDecoration: "none", color: "black" }}
          >
            <Button size="small">Más información</Button>
          </Link>
        </Switch>
      </CardActions>
    </Card>
  );
};

const ModalInfo = () => {
  const style = {
    position: "absolute",
    top: "50%",
    left: "50%",
    transform: "translate(-50%, -50%)",
    width: 1000,
    bgcolor: "background.paper",
    border: "2px solid #000",
    boxShadow: 24,
    p: 4,
  };

  const [open, setOpen] = React.useState(false);
  const handleOpen = () => setOpen(true);
  const handleClose = () => setOpen(false);

  return (
    <div>
      <Button onClick={handleOpen} startIcon={<InfoIcon />} variant="text">
        {" "}
        Información sobre el Indice de calidad del Aire (ICA)
      </Button>
      <Modal
        open={open}
        onClose={handleClose}
        aria-labelledby="modal-modal-title"
        aria-describedby="modal-modal-description"
      >
        <Box sx={style}>
          <Typography variant="bold" color="text.primary" component="h2" mb={2}>
            Tabla de calidad del aire
          </Typography>
          <TablaInfo />
        </Box>
      </Modal>
    </div>
  );
};

function createData(pm10, pm25, condicion, recomendacion, protein) {
  return { pm10, pm25, condicion, recomendacion, protein };
}

const rows = [
  createData(
    "0-149",
    "0-50",
    "Buena",
    "Se puede realizar cualquier actividad al aire libre"
  ),
  createData(
    "150-194",
    "51-79",
    "Regular",
    "Las personas vulnerables deben considerar limitar esfuerzos prolongados al aire libre"
  ),
  createData(
    "195-239",
    "80-109",
    "Alerta",
    "La población de riesgo y los que realizan actividad física intensa, deben limitar los esfuerzos prolongados al aire libre"
  ),
  createData(
    "240-329",
    "110-168",
    "Pre-emergencia",
    "La población en general debe limitar el esfuerzo prolongado al aire libre y los vulnerables evitarlo"
  ),
  createData(
    " > 329",
    " > 169",
    "Emergencia",
    "La población en general debe suspender los esfuerzos al aire libre"
  ),
];

const TablaInfo = () => {
  return (
    <TableContainer component={Paper}>
      <Table sx={{ minWidth: 850 }} size="small" aria-label="a dense table">
        <TableHead>
          <TableRow>
            <TableCell sx={{ minWidth: 90 }}>MP10</TableCell>
            <TableCell sx={{ minWidth: 90 }}>MP2.5</TableCell>
            <TableCell>Condición</TableCell>
            <TableCell>Recomendaciones</TableCell>
          </TableRow>
        </TableHead>
        <TableBody>
          {rows.map((row) => (
            <TableRow key={row.name}>
              <TableCell>{row.pm10}</TableCell>
              <TableCell>{row.pm25}</TableCell>

              <TableRowColor condicion={row.condicion} />

              <TableCell>{row.recomendacion}</TableCell>
            </TableRow>
          ))}
        </TableBody>
      </Table>
    </TableContainer>
  );
};

const TableRowColor = (props) => {
  const [color, setColor] = useState("66bb6a");

  useEffect(() => {
    if (props.condicion == "Buena") {
      setColor("#66bb6a");
    }
    if (props.condicion == "Regular") {
      setColor("#fdd835");
    }
    if (props.condicion == "Alerta") {
      setColor("#ff9800");
    }
    if (props.condicion == "Pre-emergencia") {
      setColor("#f44336");
    }
    if (props.condicion == "Emergencia") {
      setColor("#9c27b0");
    }
  }, []);

  return (
    <TableCell sx={{ backgroundColor: color, color: "white" }}>
      {props.condicion}
    </TableCell>
  );
};

const Listadispositvos = () => {
  const [dispositivos, setDispositivos] = useState("");
  const [cargados, setCargados] = useState(false);

  const auth = useAuth();

  const getDispositivos = async () => {
    await axios
      .get(dispMed, { headers: { "x-access-token": auth.token } })
      .then(async (response) => {
        console.log(response.data);
        setDispositivos(response.data);
        // console.log(response);
      })
      .finally(() => {
        setCargados(true);
      });
  };

  useEffect(() => {
    //await verifica
    getDispositivos();
  }, []);
  return (
    <Box sx={{ flexGrow: 1 }}>
      <Typography variant="bold" color="text.primary" component="h1">
        Dispositivos para la medición de calidad del aire
      </Typography>
      <ModalInfo />

      <Typography
        variant="body1"
        color="text.secondary"
        component="h2"
      ></Typography>

      {cargados && (
        <ListDispositivos dispositivos={dispositivos}></ListDispositivos>
      )}

      {!cargados && <LinearProgress />}
    </Box>
  );
};

const ListDispositivos = (prop) => {
  return (
    <Grid
      container
      spacing={{ xs: 2, md: 3, mt: 2 }}
      columns={{ xs: 12, sm: 8, md: 12 }}
    >
      {prop.dispositivos.length === 0 && (
        <Grid item xs={12} sm={12} md={12} mt={2}>
          <Typography variant="bold" color="text.primary" component="h2">
            No se encontraron dispositivos con mediciones registradas ⚠️
          </Typography>
        </Grid>
      )}

      {Array.from(Array(prop.dispositivos.length)).map((_, index) => (
        //preguntar la ultima medición de este dispositivo

        <Grid item xs={12} sm={4} md={4} key={index} mt={2}>
          {prop.dispositivos[index].medicion && (
            <CardItem
              nombre={prop.dispositivos[index].dispositivo.nombre}
              pm25={prop.dispositivos[index].medicion.pm25}
              id={prop.dispositivos[index].dispositivo._id}
            />
          )}
        </Grid>
      ))}
    </Grid>
  );
};

const Mensaje = (prop) => {
  const [mensaje, setMensaje] = useState(
    "Se puede realizar cualquier actividad al aire libre"
  );

  useEffect(() => {
    if (prop.pm25 < 50) {
      setMensaje("Se puede realizar cualquier actividad al aire libre");
    }
    if (prop.pm25 > 50) {
      setMensaje(
        "Las personas vulnerables deben considerar limitar esfuerzos prolongados al aire libre"
      );
    }
    if (prop.pm25 > 80) {
      setMensaje(
        "La población de riesgo y los que realizan actividad física intensa, deben limitar los esfuerzos prolongados al aire libre"
      );
    }
    if (prop.pm25 > 110) {
      setMensaje(
        "La población en general debe limitar el esfuerzo prolongado al aire libre y los vulnerables evitarlo"
      );
    }
    if (prop.pm25 > 169) {
      setMensaje(
        "La población en general debe suspender los esfuerzos al aire libre"
      );
    }
  }, []);
  return (
    <Typography variant="body2" color="text.secondary">
      {mensaje}
    </Typography>
  );
};

const Pm25 = (prop) => {
  const [color, setColor] = useState("66bb6a");

  useEffect(() => {
    if (prop.pm25 < 50) {
      setColor("#66bb6a");
    }
    if (prop.pm25 > 50) {
      setColor("#fdd835");
    }
    if (prop.pm25 > 80) {
      setColor("#ff9800");
    }
    if (prop.pm25 > 110) {
      setColor("#f44336");
    }
    if (prop.pm25 > 169) {
      setColor("#9c27b0");
    }
  }, []);

  return (
    <>
      <Box
        sx={{
          width: 100,
          height: 30,
          backgroundColor: color,
        }}
      >
        <Grid container>
          <Typography
            sx={{ p: 0.5 }}
            style={{ color: "white" }}
            align="justify"
          >
            PM 2.5:
          </Typography>
          <Typography
            sx={{ p: 0.5 }}
            style={{ color: "white" }}
            align="justify"
          >
            {prop.pm25}
          </Typography>
        </Grid>
      </Box>
    </>
  );
};

export default Listadispositvos;
