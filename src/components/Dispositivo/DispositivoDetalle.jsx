import React, { useState, useEffect } from "react";
import { createTheme } from "@mui/material/styles";
import { useParams } from "react-router-dom";

import {
  Box,
  Card,
  CardContent,
  Typography,
  Grid,
  Divider,
} from "@mui/material/";
import axios from "axios";
import Mapview from "../Mapa/MapView";
import Linearchart from "../Charts/LinearChart";

import Button from "@mui/material/Button";
import ArrowBackIcon from "@mui/icons-material/ArrowBack";

import { useHistory } from "react-router-dom";
import HorizontalTabs from "../../components/Dispositivo/HorizontalTabs";

const dispUrl =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/dispositivos/";
const promDiasAnteriores =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/mediciones/promedio/semanal/";

const ultimasCincoMediciones =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/mediciones/ultimas/";

const ultimaMedicion =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/mediciones/ultima/";

const Dispositivodetalle = () => {
  const { id } = useParams();

  const [dispositivo, setDispositivo] = useState("");
  const [promedio, setPromedio] = useState("");
  const [promedios, setPromedios] = useState("");
  const [cincoAnteriores, setCincoAnteriores] = useState("");

  useEffect(() => {
    getDispositivo();
    getPromedio();
    getPromedioDiasAnteriores();
    getCincoDiasAnteriores();
  }, []);

  const getPromedio = async () => {
    await axios
      .get(ultimaMedicion + id)
      .then((resp) => {
        // console.log(resp.data.medicion);
        setPromedio(resp.data.medicion);
      })
      .catch((error) => {
        //console.log("da error");
        // console.log(error);
      });
  };

  const getPromedioDiasAnteriores = async () => {
    await axios
      .get(promDiasAnteriores + id)
      .then((resp) => {
        //  console.log(resp.data);
        setPromedios(resp.data);
        console.log(resp.data);
      })
      .catch((error) => {
        //console.log("da error");
        // console.log(error);
      });
  };

  const getCincoDiasAnteriores = async () => {
    await axios
      .get(ultimasCincoMediciones + id)
      .then((resp) => {
        //  console.log(resp.data);
        setCincoAnteriores(resp.data);
        console.log(resp.data);
      })
      .catch((error) => {
        //console.log("da error");
        // console.log(error);
      });
  };

  const getDispositivo = async () => {
    await axios
      .get(dispUrl + id)
      .then((resp) => {
        //console.log(resp.data);

        setDispositivo(resp.data);

        //console.log("disp", dispositivo);
      })
      .catch((error) => {
        //   console.log("da error");
        //   console.log(error);
      });
  };

  const history = useHistory();
  const goBack = () => {
    history.push("/dispositivo/lista");
  };

  return (
    <div>
      <Card sx={{ minHeight: 900 }}>
        <CardContent>
          <Grid>
            <Button
              onClick={goBack}
              variant="text"
              startIcon={<ArrowBackIcon />}
            >
              Volver
            </Button>

            <Typography gutterBottom variant="h4" component="div">
              {dispositivo && dispositivo.dispositivo.nombre}
            </Typography>
          </Grid>

          <Grid container>
            <Typography gutterBottom variant="p" component="div">
              {dispositivo && dispositivo.organizacion.nombre}
            </Typography>
            <Box sx={{ m: 1 }} />
            <Divider orientation="vertical" flexItem />
            <Divider orientation="vertical" flexItem />
            <Box sx={{ m: 1 }} />
            <Typography gutterBottom variant="p" component="div">
              {dispositivo && dispositivo.grupo.nombre}
            </Typography>
          </Grid>

          {promedio && (
            <Grid container columns={12}>
              {promedio !== "" && (
                <Grid container xs={5}>
                  <Grid>
                    <Dato nombre={"PM 2.5"} valor={Math.round(promedio.pm25)} />
                  </Grid>

                  <Grid>
                    <DatoRectangulo
                      nombre={"PM 10"}
                      valor={Math.round(promedio.pm10)}
                    />
                    <DatoRectangulo
                      nombre={"Co2"}
                      valor={Math.round(promedio.co2)}
                    />
                    <DatoRectangulo
                      nombre={"T°C  "}
                      valor={Math.round(promedio.temperatura)}
                    />
                  </Grid>
                  <Grid sx={{ mt: 1, ml: 1 }}>
                    <Box borderRadius="20%">
                      {dispositivo !== "" && (
                        <Mapview
                          latitud={dispositivo.dispositivo.latitud}
                          longitud={dispositivo.dispositivo.longitud}
                        />
                      )}
                    </Box>
                  </Grid>
                </Grid>
              )}
              <Grid xs={7}>
                {/*  <Linearchart promedios={promedios} />*/}

                <HorizontalTabs
                  promedios={promedios}
                  cincoAnteriores={cincoAnteriores}
                />
              </Grid>
            </Grid>
          )}
        </CardContent>
      </Card>
    </div>
  );
};

const Dato = (prop) => {
  //condicionar color según item
  const [color, setColor] = useState("#66bb6a");

  useEffect(() => {
    if (prop.nombre === "PM 2.5") {
      if (prop.valor < 50) {
        setColor("#66bb6a");
      }
      if (prop.valor > 50) {
        setColor("#fdd835");
      }
      if (prop.valor > 80) {
        setColor("#ff9800");
      }
      if (prop.valor > 110) {
        setColor("#f44336");
      }
      if (prop.valor > 169) {
        setColor("#9c27b0");
      }
    }

    if (prop.nombre === "PM 10") {
      if (prop.valor < 149) {
        setColor("#66bb6a");
      }
      if (prop.valor > 149) {
        setColor("#fdd835");
      }
      if (prop.valor > 195) {
        setColor("#ff9800");
      }
      if (prop.valor > 240) {
        setColor("#f44336");
      }
      if (prop.valor > 329) {
        setColor("#9c27b0");
      }
    }
  }, []);

  return (
    <Box
      sx={{
        maxWidth: 220,
        minWidth: 220,
        height: 220,
        ml: 1,
        mt: 1,
        backgroundColor: color,
      }}
    >
      <Grid
        sx={{ p: 9 }}
        container
        direction="column"
        justifyContent="space-evenly"
        alignItems="center"
      >
        <Typography sx={{ pb: 1 }} style={{ color: "white" }} align="justify">
          {prop.nombre}
        </Typography>
        <Typography style={{ color: "white" }} align="justify">
          {prop.valor}
        </Typography>
      </Grid>
    </Box>
  );
};

const DatoRectangulo = (prop) => {
  //condicionar color según item
  const [color, setColor] = useState("#66bb6a");

  useEffect(() => {
    if (prop.nombre === "PM 2.5") {
      if (prop.valor < 50) {
        setColor("#66bb6a");
      }
      if (prop.valor > 50) {
        setColor("#fdd835");
      }
      if (prop.valor > 80) {
        setColor("#ff9800");
      }
      if (prop.valor > 110) {
        setColor("#f44336");
      }
      if (prop.valor > 169) {
        setColor("#9c27b0");
      }
    }

    if (prop.nombre === "PM 10") {
      if (prop.valor < 149) {
        setColor("#66bb6a");
      }
      if (prop.valor > 149) {
        setColor("#fdd835");
      }
      if (prop.valor > 195) {
        setColor("#ff9800");
      }
      if (prop.valor > 240) {
        setColor("#f44336");
      }
      if (prop.valor > 329) {
        setColor("#9c27b0");
      }
    }
  }, []);

  return (
    <Box
      sx={{
        maxWidth: 220,
        minWidth: 220,
        height: 68,
        ml: 1,
        mt: 1,
        backgroundColor: color,
      }}
    >
      <Grid sx={{ p: 3 }} container direction="row" alignItems="justify">
        <Typography variant="h6" item sx={{ ml: 3 }} style={{ color: "white" }}>
          {prop.nombre}
        </Typography>

        <Typography variant="h6" item sx={{ ml: 3 }} style={{ color: "white" }}>
          {prop.valor}
        </Typography>
      </Grid>
    </Box>
  );
};

const theme = createTheme({
  typography: {
    fontFamily: [
      "Roboto",
      '"Helvetica Neue"',
      "Arial",
      "sans-serif",
      '"Apple Color Emoji"',
      '"Segoe UI Emoji"',
      '"Segoe UI Symbol"',
    ].join(","),
  },
});

export default Dispositivodetalle;
