import React, { useState, useEffect } from "react";
import { styled, useTheme } from "@mui/material/styles";
import Box from "@mui/material/Box";
import MuiDrawer from "@mui/material/Drawer";
import MuiAppBar from "@mui/material/AppBar";
import Toolbar from "@mui/material/Toolbar";
import List from "@mui/material/List";
import CssBaseline from "@mui/material/CssBaseline";
import Typography from "@mui/material/Typography";
import Divider from "@mui/material/Divider";
import IconButton from "@mui/material/IconButton";
import MenuIcon from "@mui/icons-material/Menu";
import ChevronLeftIcon from "@mui/icons-material/ChevronLeft";
import ChevronRightIcon from "@mui/icons-material/ChevronRight";
//import ListItem from "@mui/material/ListItem";
import ListItemIcon from "@mui/material/ListItemIcon";
import ListItemText from "@mui/material/ListItemText";

import ListSubheader from "@mui/material/ListSubheader";
import Collapse from "@mui/material/Collapse";
import ListItemButton from "@mui/material/ListItemButton";
import ExpandLess from "@mui/icons-material/ExpandLess";
import ExpandMore from "@mui/icons-material/ExpandMore";
import StarBorder from "@mui/icons-material/StarBorder";
import SettingsIcon from "@mui/icons-material/Settings";

import MenuItem from "@mui/material/MenuItem";
import Menu from "@mui/material/Menu";

import { useHistory } from "react-router-dom";

import AccountCircle from "@mui/icons-material/AccountCircle";

import { Route, Link, Switch, useRouteMatch } from "react-router-dom";

import useAuth from "../routers/useAuth";

import Usuario from "./usuario/Usuario";
import Grupo from "./Grupo/Grupo";
import Organizacion from "./Organizacion/Organizacion";
import Dispositivo from "./Dispositivo";
import Organizaciongrupos from "./Organizacion/OrganizacionGrupos";

import GroupsIcon from "@mui/icons-material/Groups";
import PersonIcon from "@mui/icons-material/Person";
import SchoolIcon from "@mui/icons-material/School";
import SettingsInputAntennaIcon from "@mui/icons-material/SettingsInputAntenna";
import DashboardIcon from "@mui/icons-material/Dashboard";
import Changepass from "./ChangePass";
import Dispositivodetalle from "./Dispositivo/DispositivoDetalle";
import Listadispositvos from "./Dispositivo/ListaDispositvos";
import { PhoneAndroid } from "@material-ui/icons";
import Aplicacion from "./Aplicacion";
import { Button } from "@mui/material";

const drawerWidth = 240;

const openedMixin = (theme) => ({
  width: drawerWidth,
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.enteringScreen,
  }),
  overflowX: "hidden",
});

const closedMixin = (theme) => ({
  transition: theme.transitions.create("width", {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  overflowX: "hidden",
  width: `calc(${theme.spacing(7)} + 1px)`,
  [theme.breakpoints.up("sm")]: {
    width: `calc(${theme.spacing(9)} + 1px)`,
  },
});

const DrawerHeader = styled("div")(({ theme }) => ({
  display: "flex",
  alignItems: "center",
  justifyContent: "flex-end",
  padding: theme.spacing(0, 1),
  // necessary for content to be below app bar
  ...theme.mixins.toolbar,
}));

const AppBar = styled(MuiAppBar, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  zIndex: theme.zIndex.drawer + 1,
  transition: theme.transitions.create(["width", "margin"], {
    easing: theme.transitions.easing.sharp,
    duration: theme.transitions.duration.leavingScreen,
  }),
  ...(open && {
    marginLeft: drawerWidth,
    width: `calc(100% - ${drawerWidth}px)`,
    transition: theme.transitions.create(["width", "margin"], {
      easing: theme.transitions.easing.sharp,
      duration: theme.transitions.duration.enteringScreen,
    }),
  }),
}));

const Drawer = styled(MuiDrawer, {
  shouldForwardProp: (prop) => prop !== "open",
})(({ theme, open }) => ({
  width: drawerWidth,
  flexShrink: 0,
  whiteSpace: "nowrap",
  boxSizing: "border-box",
  ...(open && {
    ...openedMixin(theme),
    "& .MuiDrawer-paper": openedMixin(theme),
  }),
  ...(!open && {
    ...closedMixin(theme),
    "& .MuiDrawer-paper": closedMixin(theme),
  }),
}));

function CustomLink({ label, to, activeOnlyWhenExact, icon = <StarBorder /> }) {
  let match = useRouteMatch({
    path: to,
    exact: activeOnlyWhenExact,
  });

  return (
    <Link to={to} style={{ textDecoration: "none", color: "black" }}>
      <ListItemButton selected={match} sx={{ pl: 4 }}>
        <ListItemIcon>{icon}</ListItemIcon>
        <ListItemText primary={label} />
      </ListItemButton>
    </Link>
  );
}

export default function BaseNav() {
  const theme = useTheme();
  const [open, setOpen] = React.useState(false);

  useEffect(() => {
    //await verifica();
  }, []);

  const goPassword = () => {
    history.push("/password");
  };

  const handleDrawerOpen = () => {
    setOpen(true);
  };

  const handleDrawerClose = () => {
    setOpen(false);
  };

  const [openCollapse, setOpenCollapse] = useState(false);

  const handleClick = () => {
    setOpenCollapse(!openCollapse);
  };

  const [anchorEl, setAnchorEl] = React.useState(null);

  const handleClose = () => {
    setAnchorEl(null);
  };

  const history = useHistory();
  const auth = useAuth();
  const handleLogout = () => {
    auth.logout();
    //localStorage.removeItem('token');

    history.push("/login");
  };

  const handleMenu = (event) => {
    setAnchorEl(event.currentTarget);
  };

  return (
    <Box sx={{ display: "flex" }}>
      <CssBaseline />
      <AppBar position="fixed" open={open}>
        <Toolbar>
          <IconButton
            color="inherit"
            aria-label="open drawer"
            onClick={handleDrawerOpen}
            edge="start"
            sx={{
              marginRight: "36px",
              ...(open && { display: "none" }),
            }}
          >
            <MenuIcon />
          </IconButton>
          <Typography variant="h6" noWrap component="div" sx={{ flexGrow: 1 }}>
            Nuevo Aire
          </Typography>

          {/*Menu usuario*/}
          <div>
            <Button
              style={{
                color: "#CACACA",
                hover: { backgroundColor: "#0069d9" },
              }}
              variant="text"
              href="http://male-thumb.surge.sh/"
            >
              Docs.
            </Button>
            <IconButton
              size="large"
              aria-label="account of current user"
              aria-controls="menu-appbar"
              aria-haspopup="true"
              onClick={handleMenu}
              color="inherit"
            >
              <AccountCircle />
            </IconButton>
            <Menu
              id="menu-appbar"
              anchorEl={anchorEl}
              anchorOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              keepMounted
              transformOrigin={{
                vertical: "top",
                horizontal: "right",
              }}
              open={Boolean(anchorEl)}
              onClose={handleClose}
            >
              <MenuItem onClick={goPassword}>Cambiar password</MenuItem>
              <MenuItem onClick={handleLogout}>Cerrar Sesión</MenuItem>
            </Menu>
          </div>
        </Toolbar>
      </AppBar>
      <Drawer variant="permanent" open={open}>
        <DrawerHeader>
          <IconButton onClick={handleDrawerClose}>
            {theme.direction === "rtl" ? (
              <ChevronRightIcon />
            ) : (
              <ChevronLeftIcon />
            )}
          </IconButton>
        </DrawerHeader>
        <Divider />
        <List
          sx={{ width: "100%", maxWidth: 360, bgcolor: "background.paper" }}
          component="nav"
          aria-labelledby="nested-list-subheader"
          subheader={
            <ListSubheader
              component="div"
              id="nested-list-subheader"
            ></ListSubheader>
          }
        >
          <CustomLink
            to="/dispositivo/lista"
            label="Dispositivos"
            icon={<SettingsInputAntennaIcon />}
          />

          <CustomLink
            to="/aplicacion"
            label="Aplicación"
            icon={<PhoneAndroid />}
          />
          {auth.isModeradorOrAdmin() && (
            <ListItemButton onClick={handleClick}>
              <ListItemIcon>
                <SettingsIcon />
              </ListItemIcon>

              <ListItemText primary="Administración" />
              {openCollapse ? <ExpandLess /> : <ExpandMore />}
            </ListItemButton>
          )}

          <Collapse in={openCollapse} timeout="auto" unmountOnExit>
            <List component="div" disablePadding>
              {auth.isAdmin() && (
                <CustomLink
                  to="/usuario"
                  label="Usuarios"
                  icon={<PersonIcon />}
                />
              )}

              {auth.isModerador() && (
                <CustomLink
                  to="/usuario"
                  label="Usuarios"
                  icon={<PersonIcon />}
                />
              )}

              {auth.isModeradorOrAdmin() && (
                <CustomLink to="/grupo" label="Grupos" icon={<GroupsIcon />} />
              )}

              {auth.isAdmin() && (
                <CustomLink
                  to="/organizacion"
                  label="Organizacion"
                  icon={<SchoolIcon />}
                />
              )}

              {auth.isModerador() && (
                <CustomLink
                  to="/organizacion"
                  label="Organizacion"
                  icon={<SchoolIcon />}
                />
              )}

              <CustomLink
                to="/dispositivo"
                label="Dispositivo"
                icon={<SettingsInputAntennaIcon />}
              />
            </List>
          </Collapse>
        </List>
      </Drawer>
      <Box component="main" sx={{ flexGrow: 1, p: 3 }}>
        <DrawerHeader />

        <Switch>
          <Route exact path="/usuario" component={Usuario} />
          <Route exact path="/password" component={Changepass} />

          <Route exact path="/grupo" component={Grupo} />

          <Route exact path="/aplicacion" component={Aplicacion} />

          <Route exact path="/organizacion/" component={Organizacion} />

          <Route
            exact
            path="/organizacion/grupos/:id"
            component={Organizaciongrupos}
          />

          <Route exact path="/dispositivo" component={Dispositivo} />
          <Route exact path="/dispositivo/lista" component={Listadispositvos} />
          <Route
            exact
            path="/dispositivo/detalle/:id"
            component={Dispositivodetalle}
          />
        </Switch>
      </Box>
    </Box>
  );
}
