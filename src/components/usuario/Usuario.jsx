import React, { useEffect, useState } from "react";
import Card from "@mui/material/Card";
import axios from "axios";
import CardContent from "@mui/material/CardContent";

import Typography from "@mui/material/Typography";

import generator from "generate-password";

import {
  Toolbar,
  Tooltip,
  Button,
  IconButton,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  FormHelperText,
  Grid,
  Box,
  Chip,
} from "@mui/material/";

//import {AddIcon} from '@mui/icons-material/';
import AddIcon from "@mui/icons-material/Add";
import { Edit, Delete, Check } from "@material-ui/icons";

import Paper from "@mui/material/Paper";

import { useSnackbar } from "notistack";

import { useHistory } from "react-router-dom";

import { sha512 } from "js-sha512";

import {
  Table,
  TableContainer,
  TableHead,
  TableCell,
  TableBody,
  TableRow,
  Modal,
  TextField,
  FormGroup,
} from "@material-ui/core";
import useAuth from "../../routers/useAuth";

const cardStyle = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const baseUrl =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/usuarios/";
const baseUrlAlumnos =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/alumnos/";

const orgUrl =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/organizaciones/";

export default function Usuario() {
  const [usuarios, setUsuarios] = useState([]);
  const [modalInsertar, setModalInsertar] = useState(false);
  const [modalEditar, setModalEditar] = useState(false);
  const [modalEliminar, setModalEliminar] = useState(false);
  const [modalActivar, setModalActivar] = useState(false);

  const [organizaciones, setOrganizaciones] = useState([]);

  const [organizacionSeleccionada, setOrganizacionSeleccionada] =
    useState(null);
  const [organizacionFiltrar, setOrganizacionFiltrar] = useState(null);

  const [grupos, setGrupos] = useState([]);
  const [gruposFiltrar, setGruposFiltrar] = useState(null);
  const [grupoSeleccionado, setGrupoSeleccionado] = useState(null);
  const [grupoFiltrar, setGrupoFiltrar] = useState("");

  const [rolSeleccionado, setRolSeleccionado] = useState(null);
  const [generatedPass, setGeneratedPass] = useState("");

  const [usuarioSeleccionado, setUsuarioSeleccionado] = useState({
    nombre: "",
    email: "",
    password: "",
  });

  //hooks validación
  const [leyenda, setLeyenda] = useState({
    nombre: "",
    email: "",
    password: "",
    rol: "",
    orgaizacion: "",
    grupo: "",
  });
  const [errorTitulo, setErrorTitulo] = useState({
    nombre: false,
    email: false,
    password: false,
    rol: false,
    organizacion: false,
    grupo: false,
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setUsuarioSeleccionado((prevState) => ({
      ...prevState,
      [name]: value,
    }));

    //quita errores de validación
    setLeyenda((prevState) => ({
      ...prevState,
      [name]: "",
    }));

    setErrorTitulo((prevState) => ({
      ...prevState,
      [name]: false,
    }));
  };

  const handleSelectGrupo = (event) => {
    setGrupoSeleccionado(event.target.value);

    setLeyenda((prevState) => ({
      ...prevState,
      grupo: "",
    }));

    setErrorTitulo((prevState) => ({
      ...prevState,
      grupo: false,
    }));
  };

  const getOrganizaciones = async () => {
    if (auth.isAdmin()) {
      await axios.get(orgUrl).then((response) => {
        setOrganizaciones(response.data);
      });
    } else if (auth.isModerador()) {
      await axios
        .get(baseUrl + auth.user, {
          headers: { "x-access-token": auth.token },
        })
        .then((response) => {
          setOrganizaciones(response.data.organizaciones);
        });
    }
  };

  const getGrupos = async () => {
    await axios
      .get(orgUrl + organizacionSeleccionada)
      .then((response) => {
        setGrupos(response.data.grupos);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const getGruposFiltrar = async () => {
    await axios
      .get(orgUrl + organizacionFiltrar)
      .then((response) => {
        setGruposFiltrar(response.data.grupos);
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const history = useHistory();

  const handleLogout = () => {
    auth.logout();
    //localStorage.removeItem('token');

    history.push("/login");
  };

  const getUsuarios = async () => {
    if (auth.isAdmin()) {
      await axios
        .get(baseUrl, { headers: { "x-access-token": auth.token } })
        .then((response) => {
          setUsuarios(response.data);
        })
        .catch((error) => {
          console.log(error);
        });
    } else if (auth.isModerador()) {
      setUsuarios([]);

      return;
      await axios
        .get(baseUrlAlumnos, { headers: { "x-access-token": auth.token } })
        .then((response) => {
          setUsuarios(response.data);
        })
        .catch((error) => {
          console.log(error);
        });
    }
  };

  const seleccionarUsuario = (usuario, caso) => {
    setUsuarioSeleccionado(usuario);

    if (caso === "editar") {
      setModalEditar(true);
    } else {
      setModalEliminar(true);
    }
  };

  const seleccionarUsuarioActivar = (usuario) => {
    setUsuarioSeleccionado(usuario);
    setModalActivar(true);
  };

  const auth = useAuth();

  const { enqueueSnackbar } = useSnackbar();

  const handleAlert = (variant, message) => {
    enqueueSnackbar(message, { variant });
  };

  const addUsuario = async () => {
    if (rolSeleccionado == null) {
      setLeyenda((prevState) => ({
        ...prevState,
        rol: "Seleccione un rol",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        rol: true,
      }));
    } else if (organizacionSeleccionada == null) {
      setLeyenda((prevState) => ({
        ...prevState,
        organizacion: "Seleccione una organización",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        organizacion: true,
      }));
    } else if (grupoSeleccionado == null) {
      setLeyenda((prevState) => ({
        ...prevState,
        grupo: "Seleccione un grupo",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        grupo: true,
      }));
    } else if (usuarioSeleccionado.nombre.length === 0) {
      setLeyenda((prevState) => ({
        ...prevState,
        nombre: "El nombre es requerido",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        nombre: true,
      }));
    } else if (usuarioSeleccionado.email.length === 0) {
      setLeyenda((prevState) => ({
        ...prevState,
        email: "email requerido",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        email: true,
      }));
    } else {
      const { nombre, email } = usuarioSeleccionado;
      const shapass = sha512(generatedPass);

      const body = {
        organizacion: organizacionSeleccionada,
        grupo: grupoSeleccionado,
        user: {
          nombre: nombre,
          email: email,
          password: shapass,
          temp_password: generatedPass,
          roles: rolSeleccionado,
        },
      };

      await axios
        .post(baseUrl, body, { headers: { "x-access-token": auth.token } })
        .then(
          abrirCerrarAgregar(),

          setUsuarioSeleccionado({
            nombre: "",
            email: "",
          })
        )
        .then((res) => {
          console.log(res);
          handleAlert("success", res.data.message);
          getGrupo(grupoSeleccionado);
          //getUsuarios();
        })
        .catch((res) => {
          handleAlert("error", res.data.message);
        });
    }
  };

  const updateUsuario = async () => {
    if (usuarioSeleccionado.nombre.length === 0) {
      setLeyenda((prevState) => ({
        ...prevState,
        nombre: "El nombre es requerido",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        nombre: true,
      }));
    } else if (usuarioSeleccionado.email.length === 0) {
      setLeyenda((prevState) => ({
        ...prevState,
        email: "email requerido",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        email: true,
      }));
    } else {
      await axios
        .put(baseUrl + usuarioSeleccionado._id, usuarioSeleccionado, {
          headers: { "x-access-token": auth.token },
        })
        .then(
          abrirCerrarEditar(),
          setUsuarioSeleccionado({
            nombre: "",
            email: "",
          })
        )
        .then((res) => {
          handleAlert("success", res.data.message);
          getUsuarios();
        })
        .catch((res) => {
          handleAlert("error", res.data.message);
        });
    }
  };

  const activaUsuario = async () => {
    await axios
      .post(baseUrl + "activa/" + usuarioSeleccionado._id)
      .then(abrirCerrarActivar())
      .then((res) => {
        getUsuarios();
        handleAlert("success", res.data.message);
      })
      .catch((res) => {
        handleAlert("error", res.data.message);
      });
  };

  const deleteUsuario = async () => {
    await axios
      .delete(baseUrl + usuarioSeleccionado._id, {
        headers: { "x-access-token": auth.token },
      })
      .then(abrirCerrarEliminar())
      .then((res) => {
        handleAlert("success", res.data.message);
        getUsuarios();
      })
      .catch((res) => {
        handleAlert("error", res.data.message);
      });
  };

  //Gestión del modal
  const abrirCerrarAgregar = () => {
    setGrupoSeleccionado(null);
    setGrupos(null);
    setOrganizacionSeleccionada(null);
    setModalInsertar(!modalInsertar);
    setRolSeleccionado(null);
    limpiaValidacion();
    generatePassword();

    setUsuarioSeleccionado({
      nombre: "",
    });
  };

  const generatePassword = () => {
    var password = generator.generate({
      length: 6,
      numbers: true,
    });
    setGeneratedPass(password);
  };

  const abrirCerrarEliminar = () => {
    setModalEliminar(!modalEliminar);
  };

  const abrirCerrarActivar = () => {
    setModalActivar(!modalActivar);
  };

  const handleSelectRol = (event) => {
    setRolSeleccionado(event.target.value);

    setLeyenda((prevState) => ({
      ...prevState,
      rol: "",
    }));

    setErrorTitulo((prevState) => ({
      ...prevState,
      rol: false,
    }));
  };

  const handleSelectOrganizacion = (event) => {
    setGrupoSeleccionado(null);

    setOrganizacionSeleccionada(event.target.value);
    console.log(organizacionSeleccionada);

    setLeyenda((prevState) => ({
      ...prevState,
      organizacion: "",
    }));

    setErrorTitulo((prevState) => ({
      ...prevState,
      organizacion: false,
    }));
  };

  const handleSelectOrganizacionFiltrar = (event) => {
    setGrupoSeleccionado(null);
    if (event.target.value == 0) {
      setOrganizacionFiltrar(event.target.value);
      setGruposFiltrar([]);

      getUsuarios();
    } else {
      setOrganizacionFiltrar(event.target.value);
      console.log(organizacionFiltrar);
    }
  };

  const handleSelectGrupoFiltrar = async (event) => {
    const grupo_id = event.target.value;
    await setGrupoFiltrar(grupo_id);

    console.log(event.target.value, grupoFiltrar);

    //traer grupo u sus alumnos
    getGrupo(event.target.value);
  };

  const getGrupo = async (grupo_id) => {
    await axios
      .get(
        "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/grupos/" +
          grupo_id,
        { headers: { "x-access-token": auth.token } }
      )
      .then((response) => {
        //TODO:LLAMAR A FUNCION QUE TRAE A USUARIOS DE ESE

        setUsuarios(response.data.users);
        //console.log(response.data)
      })
      .catch((error) => {
        console.log(error);
      });
  };

  const limpiaValidacion = () => {
    setLeyenda({
      nombre: "",
    });

    setErrorTitulo({
      nombre: false,
    });
  };

  const abrirCerrarEditar = () => {
    setModalEditar(!modalEditar);
  };

  useEffect(() => {
    getUsuarios();
    getOrganizaciones();
  }, []);

  useEffect(async () => {
    getGrupos();
  }, [organizacionSeleccionada]);

  useEffect(async () => {
    getGruposFiltrar();
  }, [organizacionFiltrar]);

  const bodyEditar = (
    <div>
      <Box sx={cardStyle}>
        <h3>Editar Usuario</h3>
        <FormGroup>
          <TextField
            variant="outlined"
            label="Nombre"
            name="nombre"
            onChange={handleChange}
            helperText={leyenda.nombre}
            error={errorTitulo.nombre}
            value={usuarioSeleccionado && usuarioSeleccionado.nombre}
          />
          <br />
          <TextField
            variant="outlined"
            label="E-Mail"
            name="email"
            onChange={handleChange}
            helperText={leyenda.email}
            error={errorTitulo.email}
            value={usuarioSeleccionado && usuarioSeleccionado.email}
          />
          <br />

          <Button onClick={updateUsuario} color="success">
            Actualizar
          </Button>
          <Button onClick={abrirCerrarEditar} color="error">
            Cerrar
          </Button>
        </FormGroup>
      </Box>
    </div>
  );

  const bodyInsertar = (
    <div>
      <Box sx={cardStyle}>
        <h3>Agregar Usuario</h3>
        <FormGroup>
          <FormControl margin="normal" required error={errorTitulo.rol}>
            <InputLabel id="org-select-label">Rol</InputLabel>
            <Select
              margin="normal"
              labelId="rol-select-label"
              id="rol-simple-select"
              value={rolSeleccionado}
              label="Rol"
              onChange={handleSelectRol}
            >
              <MenuItem key={"admin"} value={"admin"}>
                Administrador
              </MenuItem>
              <MenuItem key={"moderador"} value={"moderador"}>
                Moderador
              </MenuItem>
              <MenuItem key={"user"} value={"user"}>
                Usuario
              </MenuItem>
            </Select>
            {errorTitulo.rol && (
              <FormHelperText error>{leyenda.rol}</FormHelperText>
            )}
          </FormControl>

          <FormControl
            margin="normal"
            required
            error={errorTitulo.organizacion}
          >
            <InputLabel id="org-select-label">Organización</InputLabel>

            <Select
              margin="normal"
              labelId="org-select-label"
              id="demo-simple-select"
              value={organizacionSeleccionada}
              label="Organización"
              onChange={handleSelectOrganizacion}
            >
              {organizaciones.map((element) => (
                <MenuItem key={element._id} value={element._id}>
                  {element.nombre}
                </MenuItem>
              ))}
            </Select>
            {errorTitulo.organizacion && (
              <FormHelperText>{leyenda.organizacion}</FormHelperText>
            )}
          </FormControl>

          <FormControl margin="normal" required error={errorTitulo.grupo}>
            <InputLabel id="grupo-select-label">Grupo</InputLabel>

            <Select
              labelId="grupo-select-label"
              id="grupo-simple-select"
              value={grupoSeleccionado}
              label="Grupo"
              onChange={handleSelectGrupo}
            >
              {grupos &&
                grupos.map((element) => (
                  <MenuItem key={element._id} value={element._id}>
                    {element.nombre}
                  </MenuItem>
                ))}
            </Select>
            {errorTitulo.grupo && (
              <FormHelperText>{leyenda.grupo}</FormHelperText>
            )}
          </FormControl>

          <TextField
            margin="normal"
            variant="outlined"
            label="Nombre"
            required
            name="nombre"
            onChange={handleChange}
            helperText={leyenda.nombre}
            error={errorTitulo.nombre}
          />

          <TextField
            margin="normal"
            variant="outlined"
            label="E-Mail"
            name="email"
            required
            onChange={handleChange}
            helperText={leyenda.email}
            error={errorTitulo.email}
            autoComplete="email"
          />

          <TextField
            margin="normal"
            variant="outlined"
            disabled
            required
            label="Password"
            defaultValue={generatedPass}
            type="text"
            onChange={handleChange}
            autoComplete="new-password"
            helperText={leyenda.password}
            error={errorTitulo.password}
          />
          <br />

          <Button onClick={addUsuario} color="success">
            Agregar
          </Button>
          <Button onClick={abrirCerrarAgregar} color="error">
            Cerrar
          </Button>
        </FormGroup>
      </Box>
    </div>
  );

  const bodyEliminar = (
    <div>
      <Box sx={cardStyle}>
        <h3>
          ¿Seguro que desea eliminar{" "}
          {usuarioSeleccionado && usuarioSeleccionado.nombre} ?
        </h3>
        <FormGroup>
          <Button
            color="success"
            onClick={() => {
              deleteUsuario();
            }}
          >
            Aceptar
          </Button>
          <Button onClick={abrirCerrarEliminar} color="error">
            Cancelar
          </Button>
        </FormGroup>
      </Box>
    </div>
  );

  const bodyActivar = (
    <div>
      <Box sx={cardStyle}>
        <h3>
          ¿Seguro que desea activar{" "}
          {usuarioSeleccionado && usuarioSeleccionado.nombre} ?
        </h3>
        <FormGroup>
          <Button
            color="success"
            onClick={() => {
              activaUsuario();
            }}
          >
            Aceptar
          </Button>
          <Button onClick={abrirCerrarActivar} color="error">
            Cancelar
          </Button>
        </FormGroup>
      </Box>
    </div>
  );

  return (
    <>
      <Card>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            Usuarios
          </Typography>

          <TableContainer component={Paper}>
            <Toolbar>
              <Typography
                sx={{ flex: "1 1 100%" }}
                variant="h6"
                id="tableTitle"
                component="div"
              >
                Gestión de usuarios
              </Typography>

              <Tooltip title="Agregar usuario">
                <div>
                  <Button
                    onClick={abrirCerrarAgregar}
                    variant="contained"
                    startIcon={<AddIcon />}
                  >
                    Agregar
                  </Button>
                </div>
              </Tooltip>
            </Toolbar>
            <Toolbar>
              <Box sx={{ width: "100%" }} margin="normal">
                <Grid container>
                  <Grid item xs={6}>
                    <FormControl variant="standard" sx={{ minWidth: 320 }}>
                      <InputLabel id="org-select-label">
                        Seleccione una organización
                      </InputLabel>
                      <Select
                        margin="normal"
                        labelId="org-select-label"
                        id="demo-simple-select"
                        value={organizacionFiltrar}
                        onChange={handleSelectOrganizacionFiltrar}
                      >
                        {auth.isAdmin() && (
                          <MenuItem key={0} value={0}>
                            Todos
                          </MenuItem>
                        )}

                        {organizaciones.map((element, idx) => (
                          <MenuItem key={element._id} value={element._id}>
                            {element.nombre}
                          </MenuItem>
                        ))}
                      </Select>
                    </FormControl>
                  </Grid>
                  <Grid item xs={6}>
                    <FormControl variant="standard" sx={{ minWidth: 320 }}>
                      <InputLabel id="org-select-label">
                        Seleccione un grupo
                      </InputLabel>
                      <Select
                        margin="normal"
                        labelId="org-select-label"
                        id="demo-simple-select"
                        value={grupoFiltrar}
                        onChange={handleSelectGrupoFiltrar}
                      >
                        {gruposFiltrar &&
                          gruposFiltrar.map((element) => (
                            <MenuItem key={element._id} value={element._id}>
                              {element.nombre}
                            </MenuItem>
                          ))}
                      </Select>
                    </FormControl>
                  </Grid>
                </Grid>
              </Box>
            </Toolbar>

            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Nombre</TableCell>
                  <TableCell>Rol</TableCell>
                  <TableCell>E-mail</TableCell>
                  <TableCell>Password temporal</TableCell>
                  <TableCell>Estado</TableCell>
                  <TableCell>Acciones</TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {usuarios.map((element) => (
                  <TableRow key={element._id}>
                    <TableCell>{element.nombre}</TableCell>
                    <TableCell>{element.roles[0].name}</TableCell>
                    <TableCell>{element.email}</TableCell>
                    <TableCell>{element.temp_password}</TableCell>
                    <TableCell>
                      <Chip
                        label={element.estado == 1 ? "Activo" : "Inactivo"}
                        color={element.estado == 1 ? "success" : "error"}
                      />
                    </TableCell>
                    <TableCell>
                      <IconButton
                        onClick={() => {
                          seleccionarUsuario(element, "editar");
                        }}
                      >
                        <Edit color="primary" />
                      </IconButton>

                      {element.estado == 1 ? (
                        <IconButton
                          onClick={() => {
                            seleccionarUsuario(element, "eliminar");
                          }}
                        >
                          <Delete color="error" />
                        </IconButton>
                      ) : (
                        <IconButton
                          onClick={() => {
                            seleccionarUsuarioActivar(element);
                          }}
                        >
                          <Check color="success" />
                        </IconButton>
                      )}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </CardContent>
      </Card>

      <Modal
        disableEnforceFocus
        open={modalInsertar}
        onClose={abrirCerrarAgregar}
      >
        {bodyInsertar}
      </Modal>

      <Modal open={modalEditar} onClose={abrirCerrarEditar}>
        {bodyEditar}
      </Modal>

      <Modal open={modalEliminar} onClose={abrirCerrarEliminar}>
        {bodyEliminar}
      </Modal>

      <Modal open={modalActivar} onClose={abrirCerrarActivar}>
        {bodyActivar}
      </Modal>
    </>
  );
}
