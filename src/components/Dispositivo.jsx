import React, { useEffect, useState } from "react";
import Card from "@mui/material/Card";
import axios from "axios";
import CardContent from "@mui/material/CardContent";

import Typography from "@mui/material/Typography";
import { CopyToClipboard } from "react-copy-to-clipboard";

import {
  Toolbar,
  Tooltip,
  Button,
  IconButton,
  OutlinedInput,
  ClickAwayListener,
} from "@mui/material/";
import InputAdornment from "@mui/material/InputAdornment";
import {
  BrowserRouter as Router,
  Switch,
  Link,
  useHistory,
} from "react-router-dom";

//import {AddIcon} from '@mui/icons-material/';
import AddIcon from "@mui/icons-material/Add";
import { Edit, Delete, Visibility, Check } from "@material-ui/icons";
import VpnKeyIcon from "@mui/icons-material/VpnKey";
import ContentCopyIcon from "@mui/icons-material/ContentCopy";

import { useSnackbar } from "notistack";
import useAuth from "../routers/useAuth";

import {
  Table,
  TableContainer,
  TableHead,
  TableCell,
  TableBody,
  TableRow,
  Modal,
  TextField,
  Box,
  FormGroup,
} from "@material-ui/core";

import {
  Paper,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  FormHelperText,
  Chip,
} from "@mui/material/";

import { Formik } from "formik";

const cardStyle = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 600,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const baseUrl =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/dispositivos/";

const orgUrl =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/organizaciones/";
const orgUrlUser =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/usuarios/";

const dispUrlUser =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/usuarios/dispositivos/";

export default function Dispositivo() {
  const [dispositivos, setDispositivos] = useState([]);
  const [modalInsertar, setModalInsertar] = useState(false);
  const [modalEditar, setModalEditar] = useState(false);
  const [modalEliminar, setModalEliminar] = useState(false);
  const [modalActivar, setModalActivar] = useState(false);
  const [modalKey, setModalKey] = useState(false);

  const [organizaciones, setOrganizaciones] = useState([]);
  const [organizacionSeleccionada, setOrganizacionSeleccionada] =
    useState(null);

  const [dispositivoSeleccionado, setDispositivoSeleccionado] = useState({
    nombre: "",
    latitud: "",
    longitud: "",
  });

  const [grupos, setGrupos] = useState([]);
  const [grupoSeleccionado, setGrupoSeleccionado] = useState(null);

  //hooks validación

  const [leyenda, setLeyenda] = useState({
    nombre: "",
    latitud: null,
    longitud: null,
    ch_id: "",
    organizacion: "",
    grupo: "",
  });

  const [errorTitulo, setErrorTitulo] = useState({
    nombre: false,
    latitud: false,
    longitud: false,
    ch_id: false,
    organizacion: false,
    grupo: false,
  });

  const limpiaValidacion = () => {
    setLeyenda({
      nombre: "",
      latitud: "",
      longitud: "",
      ch_id: "",
    });

    setErrorTitulo({
      nombre: false,
      latitud: false,
      longitud: false,
      ch_id: false,
    });
  };

  const handleChange = (e) => {
    const { name, value } = e.target;
    setDispositivoSeleccionado((prevState) => ({
      ...prevState,
      [name]: value,
    }));

    //quita errores de validación
    setLeyenda((prevState) => ({
      ...prevState,
      [name]: "",
    }));

    setErrorTitulo((prevState) => ({
      ...prevState,
      [name]: false,
    }));
  };

  const getDispositivos = async () => {
    await axios
      .get(baseUrl, {
        headers: { "x-access-token": auth.token },
      })
      .then((response) => {
        setDispositivos(response.data);
      })
      .catch((error) => {
        console.log(error.response.status);

        if (error.response.status === 401) {
          handleLogout();
        }
      });
  };

  //TRAER orgsanizaciones
  const auth = useAuth();
  const history = useHistory();
  const handleLogout = () => {
    auth.logout();
    //localStorage.removeItem('token');

    history.push("/login");
  };

  const getOrganizaciones = async () => {
    if (auth.isAdmin()) {
      await axios.get(orgUrl).then((response) => {
        setOrganizaciones(response.data);
      });
    } else if (auth.isModerador()) {
      await axios
        .get(orgUrlUser + auth.user, {
          headers: { "x-access-token": auth.token },
        })
        .then((response) => {
          setOrganizaciones(response.data.organizaciones);
        });
    }
  };

  const handleSelect = (event) => {
    //setGrupoSeleccionado(null)

    setOrganizacionSeleccionada(event.target.value);
    console.log(organizacionSeleccionada);

    setLeyenda((prevState) => ({
      ...prevState,
      organizacion: "",
    }));

    setErrorTitulo((prevState) => ({
      ...prevState,
      organizacion: false,
    }));
  };

  const handleSelectGrupo = (event) => {
    //console.log(event.target.value);
    setGrupoSeleccionado(event.target.value);

    setLeyenda((prevState) => ({
      ...prevState,
      grupo: "",
    }));

    setErrorTitulo((prevState) => ({
      ...prevState,
      grupo: false,
    }));
  };

  const { enqueueSnackbar } = useSnackbar();

  const handleAlert = (variant, message) => {
    enqueueSnackbar(message, { variant });
  };

  const addDispositivo = async (disp) => {
    //console.log(dispositivoSeleccionado);
    console.log(disp);

    const body = {
      grupo: disp.grupo,
      dispositivo: disp,
    };
    console.log(body);

    await axios
      .post(baseUrl, body)
      .then(abrirCerrarAgregar())
      .then((res) => {
        handleAlert("success", res.data.message);
        getDispositivos();
        setOrganizacionSeleccionada(null);
        setGrupoSeleccionado(null);
      })
      .catch((res) => {
        handleAlert("error", "Error");
      });
  };

  const activaDispositivo = async () => {
    await axios
      .post(baseUrl + "activa/" + dispositivoSeleccionado._id)
      .then(abrirCerrarActivar())
      .then((res) => {
        getDispositivos();
        handleAlert("success", res.data.message);
      })
      .catch((res) => {
        handleAlert("error", res.data.message);
      });
  };

  const deleteDispositivo = async () => {
    await axios
      .delete(baseUrl + dispositivoSeleccionado._id)
      .then(abrirCerrarEliminar())

      .then((res) => {
        handleAlert("success", res.data.message);
        getDispositivos();
      })
      .catch((res) => {
        handleAlert("error", res.data.message);
        //alert("error")
      });
  };

  const updateDispositivo = async (disp) => {
    console.log(dispositivoSeleccionado);

    await axios
      .put(baseUrl + dispositivoSeleccionado._id, disp)
      .then(
        abrirCerrarEditar(),
        setDispositivoSeleccionado({
          nombre: "",
          ch_id: "",
        })
      )
      .then((res) => {
        handleAlert("success", res.data.message);
        getDispositivos();
      })
      .catch((res) => {
        handleAlert("error", res.data.message);
      });
  };

  //Gestión del modal agregar
  const abrirCerrarAgregar = () => {
    setModalInsertar(!modalInsertar);
    limpiaValidacion();
    setOrganizacionSeleccionada(null);
    setGrupoSeleccionado(null);
    setDispositivoSeleccionado({
      nombre: "",
      ch_id: "",
    });
  };

  const abrirCerrarEliminar = () => {
    setModalEliminar(!modalEliminar);
  };

  const abrirCerrarActivar = () => {
    setModalActivar(!modalActivar);
  };

  const abrirCerrarKey = () => {
    setModalKey(!modalKey);
    setOpen(false);
  };

  //Gestión del modal editar
  const abrirCerrarEditar = () => {
    setModalEditar(!modalEditar);

    limpiarCampos();
  };

  const limpiarCampos = () => {
    setDispositivoSeleccionado({
      nombre: "",
      ch_id: "",
    });
  };

  const seleccionarDispositivo = (dispositivo, caso) => {
    setDispositivoSeleccionado(dispositivo);

    if (caso === "editar") {
      setModalEditar(true);
    } else if (caso == "key") {
      setModalKey(true);
    } else {
      setModalEliminar(true);
    }
  };

  const seleccionarDispositivoActivar = (dispositivo) => {
    setDispositivoSeleccionado(dispositivo);
    setModalActivar(true);
  };

  useEffect(async () => {
    await getDispositivos();
    await getOrganizaciones();
  }, []);

  useEffect(async () => {
    getGrupos();
  }, [organizacionSeleccionada]);

  const getGrupos = async () => {
    await axios.get(orgUrl + organizacionSeleccionada).then((response) => {
      setGrupos(response.data.grupos);
    });
  };

  {
  }

  const bodyInsertar = (
    <div>
      <Box sx={cardStyle}>
        <h3>Agregar Dispositivo</h3>
        <Formik
          initialValues={{
            nombre: "",
            correo: "",
            latitud: "",
            longitud: "",
            organizacion: organizacionSeleccionada,
            grupo: grupoSeleccionado,
          }}
          validate={(valores) => {
            let errores = {};
            if (!valores.nombre) {
              errores.nombre = "Por favor ingresa un nombre";
              setErrorTitulo((prevState) => ({
                ...prevState,
                nombre: true,
              }));
            } else {
              setErrorTitulo((prevState) => ({
                ...prevState,
                nombre: false,
              }));
            }

            if (!valores.latitud) {
              errores.latitud = "Por favor ingresa una latitud";
              setErrorTitulo((prevState) => ({
                ...prevState,
                latitud: true,
              }));
            } else {
              setErrorTitulo((prevState) => ({
                ...prevState,
                latitud: false,
              }));
            }

            if (!valores.longitud) {
              errores.longitud = "Por favor ingresa una longitud";
              setErrorTitulo((prevState) => ({
                ...prevState,
                longitud: true,
              }));
            } else {
              setErrorTitulo((prevState) => ({
                ...prevState,
                longitud: false,
              }));
            }

            if (!organizacionSeleccionada) {
              errores.organizacion = "Por favor seleccione una organizacion";
              setErrorTitulo((prevState) => ({
                ...prevState,
                organizacion: true,
              }));
            } else {
              setErrorTitulo((prevState) => ({
                ...prevState,
                organizacion: false,
              }));
            }

            if (!valores.grupo) {
              errores.grupo = "Por favor seleccione un grupo";
              setErrorTitulo((prevState) => ({
                ...prevState,
                grupo: true,
              }));
            } else {
              setErrorTitulo((prevState) => ({
                ...prevState,
                grupo: false,
              }));
            }

            return errores;
          }}
          onSubmit={(valores, { resetForm }) => {
            //console.log(valores);
            // console.log("Enviado");
            addDispositivo(valores);
            resetForm();
            setOrganizacionSeleccionada(null);
          }}
        >
          {({
            values,
            errors,
            touched,
            handleSubmit,
            handleChange,
            handleBlur,
          }) => (
            <form onSubmit={handleSubmit}>
              <FormGroup>
                <TextField
                  variant="outlined"
                  label="Nombre"
                  name="nombre"
                  value={values.nombre}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={errors.nombre && touched.nombre && errors.nombre}
                  error={touched.nombre && errorTitulo.nombre}
                />
                <br />
                <TextField
                  variant="outlined"
                  label="Latitud"
                  name="latitud"
                  value={values.latitud}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={
                    touched.latitud && errors.latitud && errors.latitud
                  }
                  error={touched.latitud && errorTitulo.latitud}
                />
                <br />

                <TextField
                  variant="outlined"
                  label="Longitud"
                  name="longitud"
                  value={values.longitud}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={
                    touched.longitud && errors.longitud && errors.longitud
                  }
                  error={touched.longitud && errorTitulo.longitud}
                />
                <br />

                <FormControl
                  error={touched.organizacion && errorTitulo.organizacion}
                >
                  <InputLabel id="org-select-label">Organización</InputLabel>

                  <Select
                    labelId="org-select-label"
                    id="demo-simple-select"
                    name="organizacion"
                    value={organizacionSeleccionada}
                    label="Organización"
                    onChange={handleSelect}
                    onBlur={handleBlur}
                  >
                    {organizaciones.map((element) => (
                      <MenuItem key={element._id} value={element._id}>
                        {element.nombre}
                      </MenuItem>
                    ))}
                  </Select>
                  {errors.organizacion && touched.organizacion && (
                    <FormHelperText>{errors.organizacion}</FormHelperText>
                  )}
                </FormControl>
                <br />

                <FormControl error={touched.grupo && errorTitulo.grupo}>
                  <InputLabel id="grupo-select-label">Grupo</InputLabel>

                  <Select
                    labelId="grupo-select-label"
                    id="grupo-simple-select"
                    value={values.grupo}
                    label="Grupo"
                    name="grupo"
                    onChange={handleChange}
                    onBlur={handleBlur}
                  >
                    {grupos.map((element) => (
                      <MenuItem key={element._id} value={element._id}>
                        {element.nombre}
                      </MenuItem>
                    ))}
                  </Select>
                  {errors.grupo && touched.grupo && (
                    <FormHelperText>{errors.grupo}</FormHelperText>
                  )}
                </FormControl>
                <br />

                <Button type="submit" color="success">
                  Agregar
                </Button>
                <Button onClick={abrirCerrarAgregar} color="error">
                  Cerrar
                </Button>
              </FormGroup>
            </form>
          )}
        </Formik>
      </Box>
    </div>
  );

  const bodyEditar = (
    <div>
      <Box sx={cardStyle}>
        <h3>Editar Dispositivo</h3>

        <Formik
          initialValues={{
            nombre: dispositivoSeleccionado.nombre,

            latitud: dispositivoSeleccionado.latitud,
            longitud: dispositivoSeleccionado.longitud,
          }}
          validate={(valores) => {
            let errores = {};
            if (!valores.nombre) {
              errores.nombre = "Por favor ingresa un nombre";
              setErrorTitulo((prevState) => ({
                ...prevState,
                nombre: true,
              }));
            } else {
              setErrorTitulo((prevState) => ({
                ...prevState,
                nombre: false,
              }));
            }

            if (!valores.latitud) {
              errores.latitud = "Por favor ingresa una latitud";
              setErrorTitulo((prevState) => ({
                ...prevState,
                latitud: true,
              }));
            } else {
              setErrorTitulo((prevState) => ({
                ...prevState,
                latitud: false,
              }));
            }

            if (!valores.longitud) {
              errores.longitud = "Por favor ingresa una longitud";
              setErrorTitulo((prevState) => ({
                ...prevState,
                longitud: true,
              }));
            } else {
              setErrorTitulo((prevState) => ({
                ...prevState,
                longitud: false,
              }));
            }

            return errores;
          }}
          onSubmit={(valores, { resetForm }) => {
            console.log(valores);
            // console.log("Enviado");
            updateDispositivo(valores);

            resetForm();
          }}
        >
          {({
            values,
            errors,
            touched,
            handleSubmit,
            handleChange,
            handleBlur,
          }) => (
            <form onSubmit={handleSubmit}>
              <FormGroup>
                <TextField
                  variant="outlined"
                  label="Nombre"
                  name="nombre"
                  value={values.nombre}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={errors.nombre && touched.nombre && errors.nombre}
                  error={touched.nombre && errorTitulo.nombre}
                />
                <br />
                <TextField
                  variant="outlined"
                  label="Latitud"
                  name="latitud"
                  value={values.latitud}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={
                    touched.latitud && errors.latitud && errors.latitud
                  }
                  error={touched.latitud && errorTitulo.latitud}
                />
                <br />

                <TextField
                  variant="outlined"
                  label="Longitud"
                  name="longitud"
                  value={values.longitud}
                  onChange={handleChange}
                  onBlur={handleBlur}
                  helperText={
                    touched.longitud && errors.longitud && errors.longitud
                  }
                  error={touched.longitud && errorTitulo.longitud}
                />
                <br />

                <Button type="submit" color="success">
                  Actualizar
                </Button>
                <Button onClick={abrirCerrarEditar} color="error">
                  Cerrar
                </Button>
              </FormGroup>
            </form>
          )}
        </Formik>
      </Box>
    </div>
  );

  const bodyEliminar = (
    <div>
      <Box sx={cardStyle}>
        <h3>
          ¿Seguro que desea eliminar{" "}
          {dispositivoSeleccionado && dispositivoSeleccionado.nombre} ?
        </h3>
        <FormGroup>
          <Button
            color="success"
            onClick={() => {
              deleteDispositivo();
            }}
          >
            Aceptar
          </Button>
          <Button onClick={abrirCerrarEliminar} color="error">
            Cancelar
          </Button>
        </FormGroup>
      </Box>
    </div>
  );

  const bodyActivar = (
    <div>
      <Box sx={cardStyle}>
        <h3>
          ¿Seguro que desea activar{" "}
          {dispositivoSeleccionado && dispositivoSeleccionado.nombre} ?
        </h3>
        <FormGroup>
          <Button
            color="success"
            onClick={() => {
              activaDispositivo();
            }}
          >
            Aceptar
          </Button>
          <Button onClick={abrirCerrarActivar} color="error">
            Cancelar
          </Button>
        </FormGroup>
      </Box>
    </div>
  );

  const [open, setOpen] = React.useState(false);
  const handleTooltipClose = () => {
    setOpen(false);
  };

  const handleTooltipOpen = () => {
    setOpen(true);
  };

  const bodyKey = (
    <div>
      <Box sx={cardStyle}>
        <h3>
          Token del dispositivo{" "}
          {dispositivoSeleccionado && dispositivoSeleccionado.nombre}
        </h3>
        <FormGroup>
          <OutlinedInput
            disabled
            multiline
            id={dispositivoSeleccionado._id}
            value={dispositivoSeleccionado && dispositivoSeleccionado.token}
            // value={values.password}
            //  onChange={handleChange('password')}
            endAdornment={
              <InputAdornment position="end">
                <ClickAwayListener onClickAway={handleTooltipClose}>
                  <Tooltip
                    placement="top"
                    disableFocusListener
                    disableTouchListener
                    title="Copiar token"
                  >
                    <div>
                      <Tooltip
                        PopperProps={{
                          disablePortal: true,
                        }}
                        onClose={handleTooltipClose}
                        open={open}
                        disableFocusListener
                        disableHoverListener
                        disableTouchListener
                        title="Token copiado"
                      >
                        <div>
                          <CopyToClipboard
                            text={dispositivoSeleccionado.token}
                            onCopy={() => handleTooltipOpen()}
                          >
                            <IconButton>
                              <ContentCopyIcon
                                //onClick={handleClickShowPassword}
                                //onMouseDown={handleMouseDownPassword}
                                edge="end"
                              ></ContentCopyIcon>
                            </IconButton>
                          </CopyToClipboard>
                        </div>
                      </Tooltip>
                    </div>
                  </Tooltip>
                </ClickAwayListener>
              </InputAdornment>
            }
          />
          <br />
          <Button onClick={abrirCerrarKey} color="error">
            Cancelar
          </Button>
        </FormGroup>
      </Box>
    </div>
  );

  return (
    <>
      <Card>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            Dispositivos
          </Typography>

          <TableContainer component={Paper}>
            <Toolbar>
              <Typography
                sx={{ flex: "1 1 100%" }}
                variant="h6"
                id="tableTitle"
                component="div"
              >
                Gestión de dispositivos
              </Typography>
              {auth.isAdmin() && (
                <Tooltip title="Agregar dispositivo">
                  <div>
                    <Button
                      onClick={abrirCerrarAgregar}
                      variant="contained"
                      startIcon={<AddIcon />}
                    >
                      Agregar
                    </Button>
                  </div>
                </Tooltip>
              )}
            </Toolbar>

            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Nombre</TableCell>
                  <TableCell>ID Dispositivo</TableCell>
                  <TableCell>Estado</TableCell>
                  <TableCell>Acciones</TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {dispositivos.map((element) => (
                  <TableRow key={element._id}>
                    <TableCell>{element.nombre}</TableCell>
                    <TableCell>{element._id}</TableCell>
                    <TableCell>
                      <Chip
                        label={element.estado == 1 ? "Activo" : "Inactivo"}
                        color={element.estado == 1 ? "success" : "error"}
                      />
                    </TableCell>

                    <TableCell>
                      {auth.isAdmin() && (
                        <div>
                          <IconButton
                            onClick={() => {
                              seleccionarDispositivo(element, "key");
                            }}
                          >
                            <VpnKeyIcon
                              color="#fdd835"
                              style={{
                                color: "#fdd835",
                              }}
                            />
                          </IconButton>
                          <IconButton
                            onClick={() => {
                              seleccionarDispositivo(element, "editar");
                            }}
                          >
                            <Edit color="primary" />
                          </IconButton>

                          {element.estado == 1 ? (
                            <IconButton
                              onClick={() => {
                                seleccionarDispositivo(element, "eliminar");
                              }}
                            >
                              <Delete color="error" />
                            </IconButton>
                          ) : (
                            <IconButton
                              onClick={() => {
                                seleccionarDispositivoActivar(element);
                              }}
                            >
                              <Check color="success" />
                            </IconButton>
                          )}

                          <Switch>
                            <Link to={"/dispositivo/detalle/" + element._id}>
                              <IconButton>
                                <Visibility color="secondary" />
                              </IconButton>
                            </Link>
                          </Switch>
                        </div>
                      )}
                      {auth.isModerador() && (
                        <IconButton
                          onClick={() => {
                            seleccionarDispositivo(element, "key");
                          }}
                        >
                          <VpnKeyIcon
                            color="#fdd835"
                            style={{
                              color: "#fdd835",
                            }}
                          />
                        </IconButton>
                      )}
                      {auth.isUser() && (
                        <Switch>
                          <Link to={"/dispositivo/detalle/" + element._id}>
                            <IconButton>
                              <Visibility color="secondary" />
                            </IconButton>
                          </Link>
                        </Switch>
                      )}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </CardContent>
      </Card>

      <Modal
        disableEnforceFocus
        open={modalInsertar}
        onClose={abrirCerrarAgregar}
      >
        {bodyInsertar}
      </Modal>

      <Modal open={modalEditar} onClose={abrirCerrarEditar}>
        {bodyEditar}
      </Modal>

      <Modal open={modalEliminar} onClose={abrirCerrarEliminar}>
        {bodyEliminar}
      </Modal>

      <Modal open={modalActivar} onClose={abrirCerrarActivar}>
        {bodyActivar}
      </Modal>

      <Modal open={modalKey} onClose={abrirCerrarKey}>
        {bodyKey}
      </Modal>
    </>
  );
}
