import { React, useEffect } from "react";
import {
  LineChart,
  Line,
  XAxis,
  YAxis,
  CartesianGrid,
  Tooltip,
  Legend,
} from "recharts";

import { Typography, Grid } from "@mui/material/";

export default function CincoAnterioresChart(props) {
  return (
    <Grid container alignItems="center">
      <Typography variant="span" component="h3" ml={6} mb={5}>
        Últimas mediciones capturadas
      </Typography>

      <LineChart
        width={700}
        height={500}
        data={props.cincoAnteriores}
        margin={{
          top: 7,
          right: 30,
          left: 20,
          bottom: 7,
        }}
      >
        <CartesianGrid strokeDasharray="3 3" />
        <XAxis dataKey="hora" />
        <YAxis />
        <Tooltip />
        <Legend />
        <Line
          type="monotone"
          dataKey="pm25"
          stroke="#8884d8"
          // activeDot={{ r: 8 }}
        />

        <Line type="monotone" dataKey="pm10" stroke="#795548" />
      </LineChart>
    </Grid>
  );
}
