import React from "react";
import {
  Box,
  Card,
  Paper,
  CardContent,
  Typography,
  Grid,
  Divider,
  Link
} from "@mui/material/";
import { styled } from '@mui/material/styles';
import PhoneImg from "../assets/phone.png";
import QR from "../assets/frame.png";



const Aplicacion = () => {
  return (
    <div>
        <Card>
        <CardContent>
      <Grid container alignItems="center">

        <Grid md={6} xs={12} style={{textAlign: "right"}} >
       
        <img src={PhoneImg} width="100%" />
        
          
        </Grid>


        <Grid item md={6} xs={12} style={{textAlign: "center"}}>
            

            
        <Typography variant="alignCenter" component="h1" mb={5}>
            Escanea el código QR y obtén Nuevo Aire para tu dispositivo Android!
          </Typography>

        <img src={QR} width="25%"/>

          
        <Typography variant="alignCenter" component="h2" mt={5}>
            También puedes descargar la aplicación haciendo <Link href="https://drive.google.com/file/d/1ZTTpdj8YUkW7j0NS9fg6AscLs_nzhE5C/view?usp=sharing"  target="_blank" underline="none"> click aquí</Link>
          </Typography>


         

          

        </Grid>
      </Grid>
      </CardContent>
      </Card>
    </div>
  );
};

export default Aplicacion;
