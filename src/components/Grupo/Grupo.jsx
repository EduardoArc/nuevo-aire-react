import React, { useEffect, useState } from "react";
import Card from "@mui/material/Card";
import axios from "axios";
import CardContent from "@mui/material/CardContent";

import Typography from "@mui/material/Typography";

import { Toolbar, Tooltip, Button, IconButton, Chip } from "@mui/material/";

import {
  Paper,
  Select,
  MenuItem,
  InputLabel,
  FormControl,
  FormHelperText,
} from "@mui/material/";

//import {AddIcon} from '@mui/icons-material/';
import AddIcon from "@mui/icons-material/Add";
import { Edit, Delete, Check } from "@material-ui/icons";

import { useSnackbar } from "notistack";

import {
  Table,
  TableContainer,
  TableHead,
  TableCell,
  TableBody,
  TableRow,
  Modal,
  TextField,
  Box,
  FormGroup,
} from "@material-ui/core";

import useAuth from "../../routers/useAuth";

const cardStyle = {
  position: "absolute",
  top: "50%",
  left: "50%",
  transform: "translate(-50%, -50%)",
  width: 400,
  bgcolor: "background.paper",
  border: "2px solid #000",
  boxShadow: 24,
  p: 4,
};

const baseUrl = "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/grupos/";

const usersGroupsUrl =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/usuarios/grupos/";

const urlOrg =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/organizaciones/";
const urlOrgUsuarios =
  "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/usuarios/";

export default function Grupo() {
  const [grupos, setGrupos] = useState([]);
  const [modalInsertar, setModalInsertar] = useState(false);
  const [modalEditar, setModalEditar] = useState(false);
  const [modalEliminar, setModalEliminar] = useState(false);
  const [modalActivar, setModalActivar] = useState(false);
  const [organizaciones, setOrganizaciones] = useState([]);

  const [grupoSeleccionado, setGrupoSeleccionado] = useState({
    nombre: "",
  });

  const [organizacionSeleccionada, setOrganizacionSeleccionada] =
    useState(null);
  //const [organizacionId, setAge] = React.useState('');

  //hooks validación

  const [leyenda, setLeyenda] = useState({
    nombre: "",
    organizacion: "",
  });
  const [errorTitulo, setErrorTitulo] = useState({
    nombre: false,
    organizacion: false,
  });

  const handleChange = (e) => {
    const { name, value } = e.target;
    setGrupoSeleccionado((prevState) => ({
      ...prevState,
      [name]: value,
    }));

    //quita errores de validación
    setLeyenda((prevState) => ({
      ...prevState,
      [name]: "",
    }));

    setErrorTitulo((prevState) => ({
      ...prevState,
      [name]: false,
    }));
  };

  const handleSelect = (event) => {
    console.log(event.target.value);
    setOrganizacionSeleccionada(event.target.value);

    setLeyenda((prevState) => ({
      ...prevState,
      organizacion: "",
    }));

    setErrorTitulo((prevState) => ({
      ...prevState,
      organizacion: false,
    }));
  };

  const auth = useAuth();

  const getGrupos = async () => {
    if (auth.isAdmin()) {
      await axios.get(baseUrl).then((response) => {
        setGrupos(response.data);
      });
    } else if (auth.isModerador()) {
      await axios
        .get(usersGroupsUrl + auth.user, {
          headers: { "x-access-token": auth.token },
        })
        .then((response) => {
          setGrupos(response.data.grupos);
        });
    }
  };

  const getOrganizaciones = async () => {
    if (auth.isAdmin()) {
      await axios.get(urlOrg).then((response) => {
        setOrganizaciones(response.data);
      });
    } else if (auth.isModerador()) {
      await axios
        .get(urlOrgUsuarios + auth.user, {
          headers: { "x-access-token": auth.token },
        })
        .then((response) => {
          setOrganizaciones(response.data.organizaciones);
        });
    }
  };

  const seleccionarGrupo = (grupo, caso) => {
    setGrupoSeleccionado(grupo);

    if (caso === "editar") {
      setModalEditar(true);
    } else if ("eliminar") {
      setModalEliminar(true);
    }
  };

  const seleccionarGrupoActivar = (grupo) => {
    setGrupoSeleccionado(grupo);
    setModalActivar(true);
  };

  const addGrupo = async () => {
    if (grupoSeleccionado.nombre.length === 0) {
      setLeyenda((prevState) => ({
        ...prevState,
        nombre: "El nombre es requerido",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        nombre: true,
      }));
    } else if (organizacionSeleccionada == null) {
      setLeyenda((prevState) => ({
        ...prevState,
        organizacion: "La organizacion es requerida",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        organizacion: true,
      }));
    } else {
      const body = {
        organizacion: organizacionSeleccionada,
        grupo: grupoSeleccionado,
      };
      await axios
        .post(baseUrl, body)
        .then(
          abrirCerrarAgregar(),
          setGrupoSeleccionado({
            nombre: "",
          })
        )
        .then((res) => {
          // console.log(res);
          handleAlert("success", res.data.message);
          getGrupos();
        })
        .catch((res) => {
          handleAlert("error", res.data.message);
        });
    }
  };

  const { enqueueSnackbar } = useSnackbar();

  const handleAlert = (variant, message) => {
    enqueueSnackbar(message, { variant });
  };

  const updateGrupo = async () => {
    if (grupoSeleccionado.nombre.length === 0) {
      setLeyenda((prevState) => ({
        ...prevState,
        nombre: "El nombre es requerido",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        nombre: true,
      }));
    } else {
      await axios
        .put(baseUrl + grupoSeleccionado._id, grupoSeleccionado)
        .then(
          abrirCerrarEditar(),
          setGrupoSeleccionado({
            nombre: "",
          })
        )
        .then((res) => {
          handleAlert("success", res.data.message);
          getGrupos();
        })
        .catch((res) => {
          handleAlert("error", res.data.message);
        });
    }
  };

  const activaGrupo = async () => {
    await axios
      .post(baseUrl + "activa/" + grupoSeleccionado._id)
      .then(abrirCerrarActivar())
      .then((res) => {
        getGrupos();
        handleAlert("success", res.data.message);
      })
      .catch((res) => {
        handleAlert("error", res.data.message);
      });
  };

  const deleteGrupo = async () => {
    await axios
      .delete(baseUrl + grupoSeleccionado._id)
      .then(abrirCerrarEliminar())
      .then((res) => {
        getGrupos();
        handleAlert("success", res.data.message);
      })
      .catch((res) => {
        handleAlert("error", res.data.message);
      });
  };

  //Gestión del modal
  const abrirCerrarAgregar = () => {
    setModalInsertar(!modalInsertar);
    setOrganizacionSeleccionada(null);
    limpiaValidacion();
    setGrupoSeleccionado({
      nombre: "",
    });
  };

  const abrirCerrarEliminar = () => {
    setModalEliminar(!modalEliminar);
  };

  const abrirCerrarActivar = () => {
    setModalActivar(!modalActivar);
  };

  const limpiaValidacion = () => {
    setLeyenda({
      nombre: "",
    });

    setErrorTitulo({
      nombre: false,
    });
  };

  const abrirCerrarEditar = () => {
    setModalEditar(!modalEditar);
  };

  useEffect(async () => {
    await getGrupos();
    await getOrganizaciones();
  }, []);

  const bodyEditar = (
    <div>
      <Box sx={cardStyle}>
        <h3>Editar Grupo</h3>
        <FormGroup>
          <TextField
            variant="outlined"
            label="Nombre"
            name="nombre"
            onChange={handleChange}
            helperText={leyenda.nombre}
            error={errorTitulo.nombre}
            value={grupoSeleccionado && grupoSeleccionado.nombre}
          />
          <br />

          <Button onClick={updateGrupo} color="success">
            Actualizar
          </Button>
          <Button onClick={abrirCerrarEditar} color="error">
            Cerrar
          </Button>
        </FormGroup>
      </Box>
    </div>
  );

  /*
   */

  const bodyInsertar = (
    <div>
      <Box sx={cardStyle}>
        <h3>Agregar Grupo</h3>
        <FormGroup>
          <TextField
            variant="outlined"
            label="Nombre"
            name="nombre"
            onChange={handleChange}
            helperText={leyenda.nombre}
            error={errorTitulo.nombre}
          />
          <br />
          <FormControl error={errorTitulo.organizacion}>
            <InputLabel id="org-select-label">Organización</InputLabel>

            <Select
              labelId="org-select-label"
              id="demo-simple-select"
              value={organizacionSeleccionada}
              label="Organización"
              onChange={handleSelect}
            >
              {organizaciones.map((element) => (
                <MenuItem key={element._id} value={element._id}>
                  {element.nombre}
                </MenuItem>
              ))}
            </Select>
            {errorTitulo.organizacion && (
              <FormHelperText>{leyenda.organizacion}</FormHelperText>
            )}
          </FormControl>
          <br />

          <Button onClick={addGrupo} color="success">
            Agregar
          </Button>
          <Button onClick={abrirCerrarAgregar} color="error">
            Cerrar
          </Button>
        </FormGroup>
      </Box>
    </div>
  );

  const bodyEliminar = (
    <div>
      <Box sx={cardStyle}>
        <h3>
          ¿Seguro que desea eliminar{" "}
          {grupoSeleccionado && grupoSeleccionado.nombre} ?
        </h3>
        <FormGroup>
          <Button
            color="success"
            onClick={() => {
              deleteGrupo();
            }}
          >
            Aceptar
          </Button>
          <Button onClick={abrirCerrarEliminar} color="error">
            Cancelar
          </Button>
        </FormGroup>
      </Box>
    </div>
  );

  const bodyActivar = (
    <div>
      <Box sx={cardStyle}>
        <h3>
          ¿Seguro que desea activar{" "}
          {grupoSeleccionado && grupoSeleccionado.nombre} ?
        </h3>
        <FormGroup>
          <Button
            color="success"
            onClick={() => {
              activaGrupo();
            }}
          >
            Aceptar
          </Button>
          <Button onClick={abrirCerrarActivar} color="error">
            Cancelar
          </Button>
        </FormGroup>
      </Box>
    </div>
  );

  return (
    <>
      <Card>
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            Grupos
          </Typography>

          <TableContainer component={Paper}>
            <Toolbar>
              <Typography
                sx={{ flex: "1 1 100%" }}
                variant="h6"
                id="tableTitle"
                component="div"
              >
                Gestión de grupos
              </Typography>

              <Tooltip title="Agregar grupo">
                <Button
                  onClick={abrirCerrarAgregar}
                  variant="contained"
                  startIcon={<AddIcon />}
                >
                  Agregar
                </Button>
              </Tooltip>
            </Toolbar>

            <Table>
              <TableHead>
                <TableRow>
                  <TableCell>Nombre</TableCell>
                  <TableCell>Organización</TableCell>
                  <TableCell>Estado</TableCell>
                  <TableCell>Acciones</TableCell>
                </TableRow>
              </TableHead>

              <TableBody>
                {grupos.map((element) => (
                  <TableRow key={element._id}>
                    <TableCell>{element.nombre}</TableCell>
                    <TableCell>
                      {element.organizacion ? element.organizacion.nombre : "-"}
                    </TableCell>

                    <TableCell>
                      <Chip
                        label={element.estado == 1 ? "Activo" : "Inactivo"}
                        color={element.estado == 1 ? "success" : "error"}
                      />
                    </TableCell>

                    <TableCell>
                      <IconButton
                        onClick={() => {
                          seleccionarGrupo(element, "editar");
                        }}
                      >
                        <Edit color="primary" />
                      </IconButton>
                      {element.estado == 1 ? (
                        <IconButton
                          onClick={() => {
                            seleccionarGrupo(element, "eliminar");
                          }}
                        >
                          <Delete color="error" />
                        </IconButton>
                      ) : (
                        <IconButton
                          onClick={() => {
                            seleccionarGrupoActivar(element);
                          }}
                        >
                          <Check color="success" />
                        </IconButton>
                      )}
                    </TableCell>
                  </TableRow>
                ))}
              </TableBody>
            </Table>
          </TableContainer>
        </CardContent>
      </Card>

      <Modal
        disableEnforceFocus
        open={modalInsertar}
        onClose={abrirCerrarAgregar}
      >
        {bodyInsertar}
      </Modal>

      <Modal disableEnforceFocus open={modalEditar} onClose={abrirCerrarEditar}>
        {bodyEditar}
      </Modal>

      <Modal open={modalEliminar} onClose={abrirCerrarEliminar}>
        {bodyEliminar}
      </Modal>

      <Modal open={modalActivar} onClose={abrirCerrarActivar}>
        {bodyActivar}
      </Modal>
    </>
  );
}
