import React, { useState } from "react";
import {
  Card,
  CardContent,
  CardActions,
  Button,
  Typography,
  FormControl,
  OutlinedInput,
  InputAdornment,
  IconButton,
  InputLabel,
  FilledInput,
  FormHelperText,
} from "@mui/material/";
import axios from "axios";

import Visibility from "@mui/icons-material/Visibility";
import VisibilityOff from "@mui/icons-material/VisibilityOff";
import { sha512 } from "js-sha512";

import useAuth from "../routers/useAuth";
import { useHistory } from "react-router-dom";

const Changepass = () => {
  const [values, setValues] = useState({
    antigua: "",
    password: "",
    confirm: "",
    showAntigua: false,
    showPassword: false,
    showConfirm: false,
  });

  const [leyenda, setLeyenda] = useState({
    antigua: "",
    password: "",
    confirm: "",
  });

  const [errorTitulo, setErrorTitulo] = useState({
    antigua: false,
    password: false,
    confirm: false,
  });

  const handleChange = (prop) => (event) => {
    setValues({ ...values, [prop]: event.target.value });

    setLeyenda((prevState) => ({
      ...prevState,
      [prop]: "",
    }));

    setErrorTitulo((prevState) => ({
      ...prevState,
      [prop]: false,
    }));
  };

  const handleClickAntigua = () => {
    setValues({
      ...values,
      showAntigua: !values.showAntigua,
    });
  };

  const handleClickShowPassword = () => {
    setValues({
      ...values,
      showPassword: !values.showPassword,
    });
  };

  const handleClickShowConfirm = () => {
    setValues({
      ...values,
      showConfirm: !values.showConfirm,
    });
  };

  const handleMouseDownPassword = (event) => {
    event.preventDefault();
  };

  const auth = useAuth();
  const history = useHistory();
  const changePass = async () => {
    const { antigua, password, confirm } = values;
    //console.log(antigua, password, confirm);

    if (antigua.length == 0) {
      setLeyenda((prevState) => ({
        ...prevState,
        antigua: "Campo requerido",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        antigua: true,
      }));
    }
    if (password.length == 0) {
      setLeyenda((prevState) => ({
        ...prevState,
        password: "Campo requerido",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        password: true,
      }));
    }
    if (confirm.length == 0) {
      setLeyenda((prevState) => ({
        ...prevState,
        confirm: "Campo requerido",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        confirm: true,
      }));
    }

    if (password != confirm) {
      setLeyenda((prevState) => ({
        ...prevState,
        confirm: "Password no coincide",
      }));
      setErrorTitulo((prevState) => ({
        ...prevState,
        confirm: true,
      }));
    } else {
      if (antigua.length > 0 && password.length > 0 && confirm.length > 0) {
        const baseUrl = "http://new-air-api-dev.us-west-2.elasticbeanstalk.com/usuarios/chpass";

        const body = {
          user: auth.user,
          antigua: sha512(antigua),
          password: sha512(password),
          confirm: sha512(antigua),
        };
        //  console.log(body)

        await axios
          .post(baseUrl, body)
          .then((res) => {
            console.log(res.data);
            auth.logout();

            history.push("/login");
          })
          .catch((error) => {
           // console.log(error.response.data.message);
            if(error.response.data.message === "Password incorrecta"){
                setLeyenda((prevState) => ({
                    ...prevState,
                    antigua: "Password incorrecta",
                  }));
                  setErrorTitulo((prevState) => ({
                    ...prevState,
                    antigua: true,
                  }));
            }
          });
      }
    }
  };

  return (
    <div>
      <Card variant="outlined">
        <CardContent>
          <Typography gutterBottom variant="h5" component="div">
            Cambio de password
          </Typography>

          <FormControl sx={{ m: 1, width: "30ch" }} variant="filled">
            <InputLabel htmlFor="outlined-adornment-password">
              Password Antigua
            </InputLabel>

            <FilledInput
              error={errorTitulo.antigua}
              id="outlined-adornment-password"
              type={values.showAntigua ? "text" : "password"}
              value={values.antigua}
              onChange={handleChange("antigua")}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    name="confirm"
                    onClick={handleClickAntigua}
                    onMouseDown={handleMouseDownPassword}
                    edge="end"
                  >
                    {values.showAntigua ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
              label="Password"
            />
            {errorTitulo.antigua && (
              <FormHelperText>{leyenda.antigua}</FormHelperText>
            )}
          </FormControl>

          <FormControl sx={{ m: 1, width: "30ch" }} variant="filled">
            <InputLabel htmlFor="outlined-adornment-password">
              Nueva Password
            </InputLabel>
            <FilledInput
              error={errorTitulo.password}
              id="outlined-adornment-password"
              type={values.showPassword ? "text" : "password"}
              value={values.password}
              name="password"
              onChange={handleChange("password")}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowPassword}
                    onMouseDown={handleMouseDownPassword}
                    edge="end"
                  >
                    {values.showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
              label="Password"
            />
            {errorTitulo.password && (
              <FormHelperText>{leyenda.password}</FormHelperText>
            )}
          </FormControl>

          <FormControl sx={{ m: 1, width: "30ch" }} variant="filled">
            <InputLabel htmlFor="outlined-adornment-password">
              Confirme Password
            </InputLabel>
            <FilledInput
              error={errorTitulo.confirm}
              id="outlined-adornment-password"
              type={values.showConfirm ? "text" : "password"}
              value={values.confirm}
              name="confirm"
              onChange={handleChange("confirm")}
              endAdornment={
                <InputAdornment position="end">
                  <IconButton
                    aria-label="toggle password visibility"
                    onClick={handleClickShowConfirm}
                    onMouseDown={handleMouseDownPassword}
                    edge="end"
                  >
                    {values.showPassword ? <VisibilityOff /> : <Visibility />}
                  </IconButton>
                </InputAdornment>
              }
              label="Confirme password"
            />
            {errorTitulo.confirm && (
              <FormHelperText>{leyenda.confirm}</FormHelperText>
            )}
          </FormControl>

          <CardActions>
            <Button size="small" onClick={changePass}>
              Actualizar password
            </Button>
          </CardActions>
        </CardContent>
      </Card>
    </div>
  );
};

export default Changepass;
